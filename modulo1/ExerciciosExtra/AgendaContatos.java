import java.util.*;

public class AgendaContatos
{
   private HashMap<String, String> agenda;
   
   public AgendaContatos() {
       agenda = new LinkedHashMap<>();
  }
  
   public void adicionaContato (String nome, String numero) {
       agenda.put(nome, numero);
  }

   public String buscarNome (String nome) {
       return agenda.get(nome);
  }

   public String buscarNumero (String numero) {
       for (HashMap.Entry<String, String> par : agenda.entrySet()) {
           if (par.getValue().equals(numero)) {
               return par.getKey();
            }
        }
       return null;
  }
  
   public String agendaCsva() {
      StringBuilder builder = new StringBuilder();
      String separador = System.lineSeparator();
       for (HashMap.Entry<String, String> par : agenda.entrySet()) {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format ("%s, %s%s", chave, valor, separador);
            builder.append(contato);
      }
      return builder.toString();
  }
}
