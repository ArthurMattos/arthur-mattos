import java.util.*;
public class AtaqueIntercalado extends NoturnoPorUltimo {
    
    protected final static ArrayList<Class> ELFO_VERDE = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class
        )
    );
    
    protected final static ArrayList<Class> ELFO_NOTURNO = new ArrayList<>(
        Arrays.asList(
            ElfoNoturno.class
        )
    );

    
    @Override
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
            
        ArrayList<Elfo> arrayElfoAuxiliar = new ArrayList<>();
        arrayElfoAuxiliar.addAll(atacantes);
        
        ArrayList<Elfo> ordemAtaque = new ArrayList<>();
        int indice = 0;
        
        if(atacantes.size()%2==0){// tamanho for par a possibilidade de ter 50% para cada lado e maior
            for ( int  i = 0 ; i < atacantes.size() ; i++ ){
                if(i == 0){
                    ordemAtaque.add(atacantes.get(i));
                    arrayElfoAuxiliar.remove(i);
                    if(ELFO_VERDE.contains(ordemAtaque.get(0).getClass())){
                        indice = 1;
                    }else{
                        indice = 0;
                    }
                }
                
                for(Elfo elfo : arrayElfoAuxiliar){
                    if(elfo.getHp() > 0) {
                        if(elfo.getClass().equals(super.ELFOS_VALIDOS.get(indice))){
                            if(indice == 0){
                                indice = 1;
                            }else{
                                indice = 0;
                            }
                            ordemAtaque.add(elfo);
                            arrayElfoAuxiliar.remove(elfo);
                            break;
                        }
                    }
                }
                
            }
            return ordemAtaque;
        }
        return null;
    }
}
