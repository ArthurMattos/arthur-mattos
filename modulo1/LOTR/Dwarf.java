import java.util.*;

public class Dwarf extends Personagem {
    Item escudo =  new Item ("Escudo", 1);
    protected boolean estaEquipado;
    
   {
        this.estaEquipado = false;
   }
    
    public Dwarf(String nome,int idade) {
      super(nome, idade);
      this.hp = 110.0;
      inv.adiciona(escudo);
   }
    
    public boolean podeSofrerDano() {
        return this.hp > 0;
   }
    
    public void equipar(){
        if (this.estaEquipado == false){
            this.estaEquipado = true;
        } else if (this.estaEquipado == true) {
            this.estaEquipado = false;
        }
   }
    
    public void recebeDano () {
        if (this.podeSofrerDano()) {
        hp = hp - 10;
     } else if (this.estaEquipado == true) {
        hp = hp - 5;
     } else {
        this.status = Status.MORTO;
        System.out.println ("Dwarf derrotado!");
     }
   }

}
