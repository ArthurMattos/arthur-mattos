public class DwarfBarbaLonga extends Dwarf{
   private DadoD6 dado = new DadoD6();
    
   public DwarfBarbaLonga(String nome, int idade) {
       super(nome, idade);
    }
    
   @Override
   public void recebeDano () {
        if (dado.sortear() > 2) {
        if (this.podeSofrerDano()) {
        hp = hp - 10;
        } else if (this.estaEquipado == true) {
        hp = hp - 5;
        } else {
        this.status = Status.MORTO;
        System.out.println ("Dwarf derrotado!");
     }
    } 
   }
}
