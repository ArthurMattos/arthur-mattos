import java.util.*;

public class Elfo extends Personagem implements Atacar {
    Item flechas = new Item ("Flechas", 2);
    
    public Elfo(String nome, int idade) {
        super(nome, idade);
        inv.adiciona(new Item ("Arco", 1));
        inv.adiciona(flechas);
    }   
    
    public void atacar(Dwarf dwarf) {
        this.atiraFlecha(dwarf);
    }
    
    public int getQtdFlechas(){
        return this.flechas.getQuantidade();
    }
    
    public void setQtdFlechas(int flechas){
        this.flechas.setQuantidade(flechas);
    }

    public void compraFlechas () {
        int qtdFlechas = this.getQtdFlechas();
        double qtdOuro = this.getOuro();
        flechas.setQuantidade (qtdFlechas + 20);
        ouro = qtdOuro - 100;
    }
    
    public boolean podeAtirarFlecha(){
        return this.getQtdFlechas() > 0;
    }
    
    public void atiraFlecha(Dwarf d) {
        if (this.podeAtirarFlecha()) {
        int qtdFlechas = this.getQtdFlechas();
        super.ganhaXP();
        flechas.setQuantidade (qtdFlechas - 1);
        d.recebeDano();
     } else {
        System.out.println("Voce esta sem flechas...");
    }
    }
    
    public void derrotaInimigo () {
        if (hp > 0) {
        double qtdHp = this.getHp();
        int qtdFlechas = this.getQtdFlechas();
        double qtdOuro = this.getOuro();
        flechas.setQuantidade (qtdFlechas - 1);
        this.ouro = qtdOuro + Math.random() * 100;
        this.hp = hp - Math.random() * 100;
        super.ganhaXP();
       } else if (hp <= 0) {
            this.status = Status.MORTO;
            System.out.println("Voce morreu...");
        }
    }
    
}
