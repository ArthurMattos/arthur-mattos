import java.util.*;

public class ElfoDaLuz extends Elfo {
    private int qtdAtaques;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
           "Espada de Galvorn"
        )
    );
    
    public ElfoDaLuz (String nome, int idade) {
        super(nome, idade);
        super.ganharItem(new ItemInquebravel(DESCRICOES_OBRIGATORIAS.get(0), 1));
        this.qtdAtaques = 0;
    }
    
     private Item getEspada() {
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    @Override 
    public void removeItem( Item item ) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if( possoPerder ) {
            super.removeItem(item);
        }
    }
    
    public void atacaComEspada (Dwarf d) {
        if (this.hp > 0) {
        this.qtdAtaques = qtdAtaques + 1;
        if (qtdAtaques % 2 == 1) {
        this.hp = hp - 21;
        super.ganhaXP();
        d.recebeDano();
       } else if (qtdAtaques % 2 == 0) {
        this.hp = hp + 10;
        super.ganhaXP();
        d.recebeDano();
       }
     } else if (hp <= 0) {
            this.status = Status.MORTO;
            System.out.println("Voce morreu...");
       }
    }
}
