import java.util.*;

public class ElfoNoturno extends Elfo {
    public ElfoNoturno(String nome, int idade) {
        super(nome,idade);
    }
    
    @Override
    protected void ganhaXP() {
        this.xp = xp + Math.random() * 100 * 3;
        if (xp >= 1000) {
            nivel = nivel + 1;
            this.hp = hp + 10;
            xp = 0;
            System.out.println("Subiu de nivel!");
        }
    }
    
    @Override
    public void atiraFlecha(Dwarf d) {
        if ((this.podeAtirarFlecha()) && (hp > 0)) {
        int qtdFlechas = this.getQtdFlechas();
        super.ganhaXP();
        flechas.setQuantidade (qtdFlechas - 1);
        d.recebeDano();
        this.hp = hp - 15;
       } else if (hp <= 0) {
            this.status = Status.MORTO;
            System.out.println("Voce morreu...");
       
       } else if (podeAtirarFlecha() == false) {
        System.out.println("Voce esta sem flechas...");
      }
    }
    
     @Override
    public void derrotaInimigo () {
        if (hp > 0) {
        double qtdHp = this.getHp();
        int qtdFlechas = this.getQtdFlechas();
        double qtdOuro = this.getOuro();
        flechas.setQuantidade (qtdFlechas - 1);
        this.ouro = qtdOuro + Math.random() * 100;
        this.hp = hp - Math.random() * 100;
        this.hp = hp - 15;
        super.ganhaXP();
       } else if (hp <= 0) {
            this.status = Status.MORTO;
            System.out.println("Voce morreu...");
        }
    }
}
