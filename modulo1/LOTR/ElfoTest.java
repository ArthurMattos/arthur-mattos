

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    @Test
    public void atiraTresFlechasTendoDuasFlechas (){
        Elfo e1 = new Elfo ("Legolas", 2931);
        Dwarf d1 = new Dwarf ("Gimli");
        e1.atiraFlecha(d1);
        e1.atiraFlecha(d1);
        e1.atiraFlecha(d1);
        
        assertEquals(0, e1.getQtdFlechas());
    } 
    
    @Test 
    public void atiraFlechaDecrementaVidaDwarf (){
        Elfo e2 = new Elfo ("Legolas", 2931);
        Dwarf d2 = new Dwarf ("Gimli");
        e2.atiraFlecha(d2);
        d2.getHp2();
        assertEquals(100, (int)d2.getHp2());
    }
    }
