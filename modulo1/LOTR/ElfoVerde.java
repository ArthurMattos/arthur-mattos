import java.util.*;

public class ElfoVerde extends Elfo {
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano",
            "Arco de Vidro",
            "Flecha de Vidro"
        )
        );
    
    public ElfoVerde(String nome, int idade) {
        super(nome,idade);
   }
    
   @Override
   protected void ganhaXP() {
        this.xp = xp + Math.random() * 100 * 2;
        if (xp >= 1000) {
            nivel = nivel + 1;
            this.hp = hp + 10;
            xp = 0;
            System.out.println("Subiu de nivel!");
        }
   }
    
    private boolean isDescricaoValida(String descricao) {
        return DESCRICOES_VALIDAS.contains(descricao);
    }
     
    
   @Override
    public void ganharItem( Item item ){
        if( this.isDescricaoValida(item.getDescricao()) ) {
            this.inv.adiciona(item);
        }
    }
    
    @Override
    public void removeItem( Item item ){
        if( this.isDescricaoValida(item.getDescricao() ) ) {
            this.inv.removeItem(item);
        }
    }
}
