import java.util.*;

public class ExercitoElfos {
   ArrayList<Elfo> exercito;
   private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
   private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<> (
      Arrays.asList(
          ElfoVerde.class, 
          ElfoNoturno.class
      )    
   );
   

   public ExercitoElfos() {
       this.exercito = new ArrayList<>();
  }
    
   public ArrayList<Elfo> getExercito() {
       return this.exercito;
  }
  
   public void alistar (Elfo elfo) {
      boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
      if (podeAlistar) {
          exercito.add(elfo);
          
          ArrayList<Elfo> elfosDeUmStatus = porStatus.get(elfo.getStatus());
          if( elfosDeUmStatus == null) {
              elfosDeUmStatus = new ArrayList<>();
              porStatus.put(elfo.getStatus(), elfosDeUmStatus);
            }
          elfosDeUmStatus.add(elfo);
       }
  }
    
   public ArrayList<Elfo> buscar (Status status) {
       return this.porStatus.get(status);
  }

    public ArrayList<Elfo> contarElfos (){
        return this.exercito;
    }
}
