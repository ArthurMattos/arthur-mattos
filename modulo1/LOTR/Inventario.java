import java.util.*;

public class Inventario {
   ArrayList<Item> inv;
   
   public Inventario (){
        this.inv = new ArrayList<>();
   }
    
   public ArrayList<Item> getItens(){
       return this.inv;
    }
   
    public void adiciona(Item item) {
        this.inv.add(item);
   }
   
    public Item obter(int posicao) {
        if (posicao >= inv.size()){
           return null;
        }
        return this.inv.get(posicao);
   }
   
    public Item buscar (String descricao) {
       Item resultado = null;
        for (int i = 0; i < inv.size(); i++) {
           if (descricao.equalsIgnoreCase(inv.get(i).getDescricao())) {
               resultado = inv.get(i);
        }
    }
    return resultado;
   }
    
    public void removeItem (int posicao) {
       inv.remove(posicao);
   }
   
    public void removeItem (Item item) {
       inv.remove(item);
   }
    
    public void mostraLista () {
      StringBuilder descricoes = new StringBuilder();
        for ( int i = 0; i < this.inv.size(); i++ ){
           Item item = this.inv.get(i);
           if (item != null) { 
           String descricao = item.getDescricao();
           descricoes.append(descricao);
           boolean deveColocarVirgula = i < this.inv.size() - 1;
            if (deveColocarVirgula) {
            descricoes.append(", ");
           } 
     }
    }
  }
  
   public ArrayList<Item> inverter () {
       ArrayList<Item> listaInvertida = new ArrayList<>(this.inv.size());
       
       for (int i = this.inv.size() - 1; i >= 0 ; i-- ){
           listaInvertida.add(this.inv.get(i));
        }
        
       return listaInvertida;
   }
   
   public String mostraMaior (){
         Item maior = new Item ("x", Integer.MIN_VALUE);
         int indiceMaior = -1;
         for (int i = 0; i < inv.size(); i++) {
             if (inv.get(i).getQuantidade() > maior.getQuantidade()) {
                maior = inv.get(i);
                indiceMaior = maior.getQuantidade();
         }
     }    
     return maior.toString();
   }
   
   public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
   
   public void ordenarItens(TipoOrdenacao tipo){
       for ( int i = 0; i < this.inv.size(); i++) {
            for ( int j = 0; j < this.inv.size() - 1; j++ ) {
                Item atual = this.inv.get(j);
                Item proximo = this.inv.get(j + 1);
                boolean deveTrocar = tipo == TipoOrdenacao.ASC ?
                    atual.getQuantidade() > proximo.getQuantidade() :
                    atual.getQuantidade() < proximo.getQuantidade();
                if( deveTrocar ){
                    Item itemTrocado = atual;
                    this.inv.set(j, proximo);
                    this.inv.set(j + 1, itemTrocado);
                }
            }
    }
  }
}


