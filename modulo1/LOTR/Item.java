

public class Item {
    protected String descricao;
    protected int quantidade;
    
    public Item (String descricao, int quantidade) {
     this.descricao = descricao;
     this.quantidade = quantidade;
    }
    
    public String getDescricao () {
        return this.descricao;
    }
    
    public int getQuantidade (){
        return this.quantidade;
    }
    
    public void setDescricao (String newDescricao){
        descricao = newDescricao;
    }
    
    public void setQuantidade (int quantidade) {
        this.quantidade = quantidade;
    }
    
    public String toString(){
        return descricao; 
    }
}
