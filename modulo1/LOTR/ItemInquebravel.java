public class ItemInquebravel extends Item{
    public ItemInquebravel (String descricao, int quantidade ) {
        super(descricao, quantidade);
    }
    
    public void setQuantidade( int novaQuantidade ) {
        boolean podeAlterar = novaQuantidade > 0;
        this.quantidade = podeAlterar ? novaQuantidade : 1;
    }
}
