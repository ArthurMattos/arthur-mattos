import java.util.*;
public class NoturnoPorUltimo implements OrdemDeAtaque {
    
    protected final static ArrayList<Class> ELFOS_VALIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> ordemElfoVerde = new ArrayList<>();
        ArrayList<Elfo> ordemElfoNoturno = new ArrayList<>();
        ArrayList<Elfo> ordemAtaque = new ArrayList<>();
        
        for ( Elfo elfo : atacantes){
            boolean elfoVerde = elfo.getClass().equals(this.ELFOS_VALIDOS.get(0));
            if(elfo.getHp() > 0) {
                if(elfoVerde){
                   ordemElfoVerde.add(elfo);
                }else{
                   ordemElfoNoturno.add(elfo);
                }
            }
        }
        ordemAtaque.addAll(ordemElfoVerde);
        ordemAtaque.addAll(ordemElfoNoturno);
        return ordemAtaque;
    }
}
