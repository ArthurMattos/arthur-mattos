import java.util.*;

public interface OrdemDeAtaque {
    ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
}
