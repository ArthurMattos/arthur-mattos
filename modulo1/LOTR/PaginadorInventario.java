import java.util.*;

public class PaginadorInventario {
 private Inventario inventario;
 private int qtdPula = 0;
 
 public PaginadorInventario(Inventario inventario){
     this.inventario = inventario;
 }
 
 public void pular(int pula) {
     this.qtdPula = pula > 0 ? pula : 0;
 }
 
 public ArrayList<Item> limitar(int limite) {
    ArrayList<Item> subConjunto = new ArrayList<>();
    int fim = this.qtdPula +limite;
    for (int i = this.qtdPula ; i < fim && i < this.inventario.getItens().size(); i++ ) {
        subConjunto.add(this.inventario.obter(i));
    }
    return subConjunto;
 }
}
