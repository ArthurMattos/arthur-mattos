import java.util.*;

public abstract class Personagem {
   protected String nome; 
   protected int idade;
   protected double xp;
   protected Status status;
   protected double hp;
   protected double ouro;
   protected int nivel;
   protected Inventario inv;
   
   {
       this.status = Status.RECEM_CRIADO;
       this.hp = 100;
       this.xp = 0;
       this.ouro = 0;
       this.nivel = 1;
       this.inv = new Inventario();
   }
   
    public Personagem (String nome, int idade) {
       this.nome = nome;
       this.idade = idade;
   }
   
    public String getNome(){
        return nome;
   }
    
    public void setNome(String newNome) {
        nome = newNome;
   }
    
    public int getIdade(){
        return idade;
   }
    
    public void setIdade(int newIdade) {
        idade = newIdade;
   }
    
    public double getXp(){
        return xp;
   }
    
    public int getNivel(){
        return nivel;
   }
    
    protected void ganhaXP() {
        this.xp = xp + Math.random() * 100;
        if (xp >= 1000) {
            nivel = nivel + 1;
            this.hp = hp + 10;
            xp = 0;
            System.out.println("Subiu de nivel!");
        }
    }
   
    public Status getStatus() {
       return this.status;
   }
   
   public double getOuro() {
        return ouro;
  }
    
   public double getHp() {
        return hp;
  }
    
   public void descansa () {
        hp = 100;
  }
   
    public Inventario getInventario() {
       return this.inv;
  }
   
   public void ganharItem(Item item) {
       this.inv.adiciona(item);
  }
    
   public void removeItem(Item item) {
       this.inv.removeItem(item);
  }
}
