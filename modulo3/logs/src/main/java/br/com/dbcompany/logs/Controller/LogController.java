package br.com.dbcompany.logs.Controller;

import br.com.dbcompany.logs.Entity.LogEntity;
import br.com.dbcompany.logs.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/api/log")
public class LogController {

    @Autowired
    LogService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<LogEntity> todosLogs(){
        return service.todosOsLogs();
    }

    @GetMapping(value = "/buscar")
    @ResponseBody
    public LogEntity logPelaMensagem(@PathVariable String mensagem){
        return service.logsPelaMensagem(mensagem);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public LogEntity salvar(@RequestBody LogEntity log){
        return service.salvar(log);
    }
}
