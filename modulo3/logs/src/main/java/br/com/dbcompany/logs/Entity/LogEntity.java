package br.com.dbcompany.logs.Entity;

import javax.persistence.*;

@Entity
public class LogEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "LOG_SEQ", sequenceName = "LOG_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String mensagem;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
