package br.com.dbcompany.logs.Repository;

import br.com.dbcompany.logs.Entity.LogEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface LogRepository extends CrudRepository<LogEntity, Integer> {

    List<LogEntity> findAll ();
    LogEntity findByMensagem(String Mensagem);

}

