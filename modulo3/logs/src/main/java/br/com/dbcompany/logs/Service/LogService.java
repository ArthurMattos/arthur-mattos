package br.com.dbcompany.logs.Service;

import br.com.dbcompany.logs.Entity.LogEntity;
import br.com.dbcompany.logs.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public LogEntity salvar(LogEntity acessos) {
        return repository.save(acessos);
    }

    public List<LogEntity> todosOsLogs() {
        List<LogEntity> lstLogs = repository.findAll();
        return lstLogs;
    }

    public LogEntity logsPelaMensagem(String mensagem) {
        return repository.findByMensagem(mensagem);
    }
}
