package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Service.AgenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/agencia")
public class AgenciaController {

    @Autowired
    AgenciaService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<AgenciaEntity> todasAgencia() {
        return service.todasAsAgencias();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public AgenciaEntity salvar(@RequestBody AgenciaEntity agencia) {
        return service.salvar(agencia);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public AgenciaEntity agenciaEspecifica(@PathVariable Integer id) {
        return service.agenciaPeloId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public AgenciaEntity editarAgencia (@PathVariable Integer id, @RequestBody AgenciaEntity agencia) {
        return service.editar(agencia, id);
    }


}
