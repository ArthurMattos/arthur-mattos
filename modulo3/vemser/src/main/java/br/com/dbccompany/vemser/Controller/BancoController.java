package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import br.com.dbccompany.vemser.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/banco")
public class BancoController {

    @Autowired
    BancoService service;

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public BancoEntity bancoEspecifico(@PathVariable Integer id) {
        return service.bancoPeloId(id);
    }

}
