package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cidade")
public class CidadeController {

    @Autowired
    CidadeService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<CidadeEntity> todasCidade() {
        return service.todasAsCidades();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public CidadeEntity salvar(@RequestBody CidadeEntity cidade) {
        return service.salvar(cidade);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public CidadeEntity cidadeEspecifica(@PathVariable Integer id) {
        return service.cidadePeloId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public CidadeEntity editarCidade (@PathVariable Integer id, @RequestBody CidadeEntity cidade) {
        return service.editar(cidade, id);
    }


}
