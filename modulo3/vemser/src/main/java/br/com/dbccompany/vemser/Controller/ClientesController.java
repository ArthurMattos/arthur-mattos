package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ClientesEntity;
import br.com.dbccompany.vemser.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientesEntity> todosCliente() {
        return service.todosOsClientes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesEntity salvar(@RequestBody ClientesEntity cliente) {
        return service.salvar(cliente);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ClientesEntity clienteEspecifico(@PathVariable Integer id) {
        return service.clientesPeloId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientesEntity editarPais (@PathVariable Integer id, @RequestBody ClientesEntity cliente) {
        return service.editar(cliente, id);
    }


}
