package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Service.ConsolidacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/consolidacao")
public class ConsolidacaoController {

    @Autowired
    ConsolidacaoService service;

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ConsolidacaoEntity consolidacaoEspecifica(@PathVariable Integer id) {
        return service.consolidacaoPeloId(id);
    }
}
