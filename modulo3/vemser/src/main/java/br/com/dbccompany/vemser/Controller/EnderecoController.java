package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/endereco")
public class EnderecoController {

    @Autowired
    EnderecoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<EnderecoEntity> todosEndereco() {
        return service.todasOsEnderecos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EnderecoEntity salvar(@RequestBody EnderecoEntity endereco) {
        return service.salvar(endereco);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EnderecoEntity enderecoEspecifico(@PathVariable Integer id) {
        return service.enderecoPeloId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EnderecoEntity editarEndereco (@PathVariable Integer id, @RequestBody EnderecoEntity endereco) {
        return service.editar(endereco, id);
    }


}
