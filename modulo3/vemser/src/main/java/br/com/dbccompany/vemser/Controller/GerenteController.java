package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Service.GerenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/gerente")
public class GerenteController {

    @Autowired
    GerenteService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<GerenteEntity> todosGerente() {
        return service.todasOsGerentes();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public GerenteEntity salvar(@RequestBody GerenteEntity gerente) {
        return service.salvar(gerente);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public GerenteEntity gerenteEspecifico(@PathVariable Integer id) {
        return service.gerentePeloId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public GerenteEntity editarGerente (@PathVariable Integer id, @RequestBody GerenteEntity gerente) {
        return service.editar(gerente, id);
    }


}
