package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Service.TipoContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoconta")
public class TipoContaController {

    @Autowired
    TipoContaService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<TipoContaEntity> todosTipoConta() {
        return service.todasOsTiposDeConta();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoContaEntity salvar(@RequestBody TipoContaEntity tipoConta) {
        return service.salvar(tipoConta);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public TipoContaEntity tipoContaEspecifico(@PathVariable Integer id) {
        return service.tipoContaPeloId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public TipoContaEntity editarTipoConta (@PathVariable Integer id, @RequestBody TipoContaEntity tipoConta) {
        return service.editar(tipoConta, id);
    }


}
