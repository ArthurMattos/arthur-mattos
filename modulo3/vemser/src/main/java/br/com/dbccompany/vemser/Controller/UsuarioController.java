package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @PostMapping( value = "/novo" )
    @ResponseBody
    public UsuarioEntity salvar(@RequestBody UsuarioEntity usuario) {
        return service.salvar(usuario);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public UsuarioEntity editarUsuario (@PathVariable Integer id, @RequestBody UsuarioEntity usuario) {
        return service.editar(usuario, id);
    }


}
