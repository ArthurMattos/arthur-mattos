package br.com.dbccompany.vemser.Entity;
import javax.persistence.*;

@Entity
public class ClientesEntity  {

    @Id
    @SequenceGenerator( name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    private int id;
    private String nome;
    private String cpf;
    private String estadoCivil;
    private String dataDeNascimento;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn ( name = "ID_ENDERECO")
    private EnderecoEntity endereco;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(String dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public EnderecoEntity getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
        this.endereco = endereco;
    }

}
