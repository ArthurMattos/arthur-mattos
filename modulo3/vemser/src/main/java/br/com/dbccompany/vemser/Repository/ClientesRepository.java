package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity, Integer> {

    ClientesEntity findByNome ( String nome);
    List<ClientesEntity> findAllByNome( String nome);
    List<ClientesEntity> findAll();
    ClientesEntity findById (int id);
}
