package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Integer> {

    EnderecoEntity findByLogradouro (String logradouro);
    List<EnderecoEntity> findAllByLogradouro (String logradouro);
    EnderecoEntity findByNumero (int numero);
    EnderecoEntity findByCep (char cep);
    List<EnderecoEntity> findAll();
    EnderecoEntity findById (int id);
}
