package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AgenciaService {

    @Autowired
    private AgenciaRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public AgenciaEntity salvar( AgenciaEntity agencia ) {
        return repository.save(agencia);
    }

    @Transactional( rollbackFor = Exception.class )
    public AgenciaEntity editar( AgenciaEntity agencia, int id ) {
        agencia.setId(id);
        return repository.save(agencia);
    }

    public List<AgenciaEntity> todasAsAgencias() {
        List<AgenciaEntity> lstAgencia = repository.findAll();
        return lstAgencia;
    }

    public AgenciaEntity agenciaPeloId( int id ) {
        return repository.findById(id);
    }

}