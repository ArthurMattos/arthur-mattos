package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import br.com.dbccompany.vemser.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BancoService {

    @Autowired
    private BancoRepository repository;

    /*@Transactional( rollbackFor = Exception.class )
    public BancoEntity salvar( BancoEntity banco ) {
        return repository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public BancoEntity editar( BancoEntity banco, int id ) {
        banco.setId(id);
        return repository.save(banco);
    } */

    public BancoEntity bancoPeloId( int id ) {
        return repository.findById(id);
    }

}