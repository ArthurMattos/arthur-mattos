package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CidadeService {

    @Autowired
    private CidadeRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public CidadeEntity salvar( CidadeEntity cidade ) {
        return repository.save(cidade);
    }

    @Transactional( rollbackFor = Exception.class )
    public CidadeEntity editar( CidadeEntity cidade, int id ) {
        cidade.setId(id);
        return repository.save(cidade);
    }

    public List<CidadeEntity> todasAsCidades() {
        List<CidadeEntity> lstCidade = repository.findAll();
        return lstCidade;
    }

    public CidadeEntity cidadePeloId( int id ) {
        return repository.findById(id);
    }

}