package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ClientesEntity;
import br.com.dbccompany.vemser.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ClientesEntity salvar( ClientesEntity clientes ) {
        return repository.save(clientes);
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientesEntity editar( ClientesEntity clientes, int id ) {
        clientes.setId(id);
        return repository.save(clientes);
    }

    public List<ClientesEntity> todosOsClientes() {
        List<ClientesEntity> lstClientes = repository.findAll();
        return lstClientes;
    }

    public ClientesEntity clientesPeloId( int id ) {
        return repository.findById(id);
    }

}