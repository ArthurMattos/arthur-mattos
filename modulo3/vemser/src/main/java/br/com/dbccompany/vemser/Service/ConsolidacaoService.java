package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Repository.ConsolidacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ConsolidacaoService {

    @Autowired
    private ConsolidacaoRepository repository;

    /* @Transactional( rollbackFor = Exception.class )
    public ConsolidacaoEntity salvar( ConsolidacaoEntity consolidacao ) {
        return repository.save(consolidacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public ConsolidacaoEntity editar( ConsolidacaoEntity consolidacao, int id ) {
        agencia.setId(id);
        return repository.save(consolidacao);
    } */

    public ConsolidacaoEntity consolidacaoPeloId( int id ) {
        return repository.findById(id);
    }

}