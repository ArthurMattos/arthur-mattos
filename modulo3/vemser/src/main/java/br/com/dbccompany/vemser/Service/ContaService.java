package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntityId;
import br.com.dbccompany.vemser.Repository.ContaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContaService extends ServiceAbstract<ContaRepository, ContaEntity, ContaEntityId> {

    @Transactional( rollbackFor = Exception.class )
    public ContaEntity editar( ContaEntity entidade, Integer id, Integer idTipoConta ) {
        ContaEntityId newId = new ContaEntityId(id, idTipoConta);
        entidade.setId(newId);
        return repository.save(entidade);
    }

}
