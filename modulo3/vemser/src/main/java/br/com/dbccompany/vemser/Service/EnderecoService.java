package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EnderecoService {

    @Autowired
    private EnderecoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public EnderecoEntity salvar( EnderecoEntity endereco ) {
        return repository.save(endereco);
    }

    @Transactional( rollbackFor = Exception.class )
    public EnderecoEntity editar( EnderecoEntity endereco, int id ) {
        endereco.setId(id);
        return repository.save(endereco);
    }

    public List<EnderecoEntity> todasOsEnderecos() {
        List<EnderecoEntity> lstEndereco = (List<EnderecoEntity>) repository.findAll();
        return lstEndereco;
    }

    public EnderecoEntity enderecoPeloId( int id ) {
        return repository.findById(id);
    }

}