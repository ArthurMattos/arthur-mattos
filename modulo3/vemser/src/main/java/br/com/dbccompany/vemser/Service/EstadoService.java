package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EstadoService {

    @Autowired
    private EstadoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public EstadoEntity salvar( EstadoEntity estado ) {
        return repository.save(estado);
    }

    @Transactional( rollbackFor = Exception.class )
    public EstadoEntity editar( EstadoEntity estado, int id ) {
        estado.setId(id);
        return repository.save(estado);
    }

    public List<EstadoEntity> todosOsEstados() {
        List<EstadoEntity> lstEstado = repository.findAll();
        return lstEstado;
    }

    public EstadoEntity estadoPeloId( int id ) {
        return repository.findById(id);
    }

}