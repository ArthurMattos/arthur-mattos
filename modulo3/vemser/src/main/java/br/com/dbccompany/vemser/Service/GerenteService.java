package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Repository.GerenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GerenteService {

    @Autowired
    private GerenteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public GerenteEntity salvar( GerenteEntity gerente ) {
        return repository.save(gerente);
    }

    @Transactional( rollbackFor = Exception.class )
    public GerenteEntity editar( GerenteEntity gerente, int id ) {
        gerente.setId(id);
        return repository.save(gerente);
    }

    public List<GerenteEntity> todasOsGerentes() {
        List<GerenteEntity> lstGerente = repository.findAll();
        return lstGerente;
    }

    public GerenteEntity gerentePeloId( int id ) {
        return repository.findById(id);
    }


}