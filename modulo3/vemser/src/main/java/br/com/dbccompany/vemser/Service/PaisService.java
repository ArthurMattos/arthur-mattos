package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PaisService {

    @Autowired
    private PaisRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public PaisEntity salvar( PaisEntity pais ) {
        return repository.save(pais);
    }

    @Transactional( rollbackFor = Exception.class )
    public PaisEntity editar( PaisEntity pais, int id ) {
        pais.setId(id);
        return repository.save(pais);
    }

    public List<PaisEntity> todosOsPaises() {
        List<PaisEntity> lstPaises = repository.findAll();
        return lstPaises;
    }

    public PaisEntity paisPeloId( int id ) {
        return repository.findById(id);
    }

}