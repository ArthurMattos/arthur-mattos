package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Repository.TipoContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContaService {

    @Autowired
    private TipoContaRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public TipoContaEntity salvar( TipoContaEntity tipoConta ) {
        return repository.save(tipoConta);
    }

    @Transactional( rollbackFor = Exception.class )
    public TipoContaEntity editar( TipoContaEntity tipoConta, int id ) {
        tipoConta.setId(id);
        return repository.save(tipoConta);
    }

    public List<TipoContaEntity> todasOsTiposDeConta() {
        List<TipoContaEntity> lstTipoConta = repository.findAll();
        return lstTipoConta;
    }

    public TipoContaEntity tipoContaPeloId( int id ) {
        return repository.findById(id);
    }

}