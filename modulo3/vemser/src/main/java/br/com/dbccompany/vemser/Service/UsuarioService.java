package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity salvar( UsuarioEntity usuario ) {
        return repository.save(usuario);
    }

    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity editar( UsuarioEntity usuario, int id ) {
        usuario.setId(id);
        return repository.save(usuario);
    }

}