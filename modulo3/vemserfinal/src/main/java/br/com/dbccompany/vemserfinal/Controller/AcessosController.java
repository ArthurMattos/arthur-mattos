package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.AcessosEntity;
import br.com.dbccompany.vemserfinal.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<AcessosEntity> todosAcessos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public AcessosEntity salvar(@RequestBody AcessosEntity acessos) {
        return service.salvar(acessos);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public AcessosEntity acessosEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public AcessosEntity editarAcessos (@PathVariable Integer id, @RequestBody AcessosEntity acessos) {
        return service.editar(acessos, id);
    }


}
