package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.ClientesEntity;
import br.com.dbccompany.vemserfinal.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientesEntity> todosClintes() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesEntity salvar(@RequestBody ClientesEntity clientes) {
        return service.salvar(clientes);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ClientesEntity clientesEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientesEntity editarClientes (@PathVariable Integer id, @RequestBody ClientesEntity clientes) {
        return service.editar(clientes, id);
    }


}