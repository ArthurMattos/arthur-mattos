package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.ClientesPacotesEntity;
import br.com.dbccompany.vemserfinal.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {

    @Autowired
    ClientesPacotesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientesPacotesEntity> todosAcessos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ClientesPacotesEntity salvar(@RequestBody ClientesPacotesEntity clientesPacotes) {
        return service.salvar(clientesPacotes);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ClientesPacotesEntity clientesPacotesEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientesPacotesEntity editarClientesPacotes (@PathVariable Integer id, @RequestBody ClientesPacotesEntity clientesPacotes) {
        return service.editar(clientesPacotes, id);
    }


}