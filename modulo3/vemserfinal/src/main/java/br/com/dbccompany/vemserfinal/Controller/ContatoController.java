package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.ContatoEntity;
import br.com.dbccompany.vemserfinal.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contatos")
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ContatoEntity> todosContatos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContatoEntity salvar(@RequestBody ContatoEntity contatos) {
        return service.salvar(contatos);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ContatoEntity contatosEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ContatoEntity editarContatos (@PathVariable Integer id, @RequestBody ContatoEntity contatos) {
        return service.editar(contatos, id);
    }


}