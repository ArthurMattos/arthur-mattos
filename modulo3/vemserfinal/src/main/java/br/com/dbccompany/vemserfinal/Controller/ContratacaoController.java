package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.ContratacaoEntity;
import br.com.dbccompany.vemserfinal.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ContratacaoEntity> todasContratacoes() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContratacaoEntity salvar(@RequestBody ContratacaoEntity contratacao) {
        return service.salvar(contratacao);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public ContratacaoEntity contratacaoEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ContratacaoEntity editarContratacao (@PathVariable Integer id, @RequestBody ContratacaoEntity contratacao) {
        return service.editar(contratacao, id);
    }


}