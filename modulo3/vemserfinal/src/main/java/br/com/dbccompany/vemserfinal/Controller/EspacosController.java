package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.EspacosEntity;
import br.com.dbccompany.vemserfinal.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    EspacosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<EspacosEntity> todosEspacos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EspacosEntity salvar(@RequestBody EspacosEntity espacos) {
        return service.salvar(espacos);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EspacosEntity espacosEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacosEntity editarEspacos (@PathVariable Integer id, @RequestBody EspacosEntity espacos) {
        return service.editar(espacos, id);
    }


}