package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.EspacosPacotesEntity;
import br.com.dbccompany.vemserfinal.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacosPacotes")
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<EspacosPacotesEntity> todosEspacosPacotes() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EspacosPacotesEntity salvar(@RequestBody EspacosPacotesEntity espacosPacotes) {
        return service.salvar(espacosPacotes);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EspacosPacotesEntity espacosPacotesEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacosPacotesEntity editarEspacosPacotes (@PathVariable Integer id, @RequestBody EspacosPacotesEntity espacosPacotes) {
        return service.editar(espacosPacotes, id);
    }


}