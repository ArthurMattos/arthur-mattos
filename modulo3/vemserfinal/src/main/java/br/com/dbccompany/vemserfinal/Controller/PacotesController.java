package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.PacotesEntity;
import br.com.dbccompany.vemserfinal.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    PacotesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<PacotesEntity> todosPacotes() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public PacotesEntity salvar(@RequestBody PacotesEntity pacotes) {
        return service.salvar(pacotes);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public PacotesEntity pacotesEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public PacotesEntity editarPacotes (@PathVariable Integer id, @RequestBody PacotesEntity pacotes) {
        return service.editar(pacotes, id);
    }


}