package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.PagamentosEntity;
import br.com.dbccompany.vemserfinal.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<PagamentosEntity> todosPagamentos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public PagamentosEntity salvar(@RequestBody PagamentosEntity pagamentos) {
        return service.salvar(pagamentos);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public PagamentosEntity pagamentosEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public PagamentosEntity editarPagamentos (@PathVariable Integer id, @RequestBody PagamentosEntity pagamentos) {
        return service.editar(pagamentos, id);
    }


}