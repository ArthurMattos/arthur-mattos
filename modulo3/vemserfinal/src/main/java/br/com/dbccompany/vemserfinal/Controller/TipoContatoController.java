package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.TipoContatoEntity;
import br.com.dbccompany.vemserfinal.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<TipoContatoEntity> todosTipoContatos() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoContatoEntity salvar(@RequestBody TipoContatoEntity tipoContato) {
        return service.salvar(tipoContato);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public TipoContatoEntity tipoContatoEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public TipoContatoEntity editarTiposDeContato (@PathVariable Integer id, @RequestBody TipoContatoEntity tipoContato) {
        return service.editar(tipoContato, id);
    }


}