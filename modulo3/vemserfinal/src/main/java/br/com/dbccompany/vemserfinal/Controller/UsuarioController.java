package br.com.dbccompany.vemserfinal.Controller;

import br.com.dbccompany.vemserfinal.Entity.UsuariosEntity;
import br.com.dbccompany.vemserfinal.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuariosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<UsuariosEntity> todosUsuarios() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public UsuariosEntity salvar(@RequestBody UsuariosEntity usuario) {
        return service.salvar(usuario);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public UsuariosEntity usuarioEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public UsuariosEntity editarUsuarios (@PathVariable Integer id, @RequestBody UsuariosEntity usuario) {
        return service.editar(usuario, id);
    }


}