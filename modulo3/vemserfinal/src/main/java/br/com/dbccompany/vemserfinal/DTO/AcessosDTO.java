package br.com.dbccompany.vemserfinal.DTO;

import br.com.dbccompany.vemserfinal.Entity.AcessosEntity;
import br.com.dbccompany.vemserfinal.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDate;
import java.time.Period;

public class AcessosDTO {
    private Integer id;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;
    private boolean isEntrada;
    private LocalDate data;
    private boolean isExcecao;

    public AcessosDTO() {}

    public AcessosDTO(AcessosEntity acessos) {
        this.id = acessos.getId();
        this.saldoCliente = acessos.getSaldoCliente();
        this.isEntrada = acessos.isIs_entrada();
        this.data = acessos.getData();
        this.isExcecao = acessos.isIs_excecao();
        if(isEntrada){
            boolean dataVencimento = this.saldoCliente.getVencimento().compareTo(LocalDate.now()) > 0;
            if(!dataVencimento || saldoCliente.getQuantidade() <= 0){
                System.out.println("Saldo Insuficiente");
            } else {
                Period period = Period.between(this.saldoCliente.getVencimento(), LocalDate.now());
                System.out.println("Ainda restam " + period.getDays() + " dias.");
            }
        }
    }

    public AcessosEntity convert() {
        AcessosEntity acesso = new AcessosEntity();
        acesso.setId(this.id);
        acesso.setSaldoCliente(this.saldoCliente);
        acesso.setIs_entrada(this.isEntrada);
        acesso.setData(this.data);
        acesso.setIs_excecao(this.isExcecao);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}