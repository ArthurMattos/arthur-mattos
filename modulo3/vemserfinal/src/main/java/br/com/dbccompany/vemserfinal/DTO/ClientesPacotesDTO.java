package br.com.dbccompany.vemserfinal.DTO;

import br.com.dbccompany.vemserfinal.Entity.ClientesEntity;
import br.com.dbccompany.vemserfinal.Entity.ClientesPacotesEntity;
import br.com.dbccompany.vemserfinal.Entity.PacotesEntity;
import br.com.dbccompany.vemserfinal.Entity.PagamentosEntity;

import java.util.List;

public class ClientesPacotesDTO {

    private Integer id;
    private ClientesEntity cliente;
    private PacotesEntity pacote;
    private int quantidade;
    private List<PagamentosEntity> pagamentos;

    public ClientesPacotesDTO() {}

    public ClientesPacotesDTO(ClientesPacotesEntity clientePacote) {
        this.id = clientePacote.getId();
        this.cliente = clientePacote.getClientes();
        this.pacote = clientePacote.getPacotes();
        this.quantidade = clientePacote.getQuantidade();
        this.pagamentos = clientePacote.getPagamentos();
    }

    public ClientesPacotesEntity convert() {
        ClientesPacotesEntity clientePacote = new ClientesPacotesEntity();
        if(this.cliente != null || this.pacote != null){
            clientePacote.setId(this.id);
            clientePacote.setClientes(this.cliente);
            clientePacote.setPacotes(this.pacote);
            clientePacote.setQuantidade(this.quantidade);
            clientePacote.setPagamentos(this.pagamentos);
        }
        return clientePacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}