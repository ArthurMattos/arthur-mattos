package br.com.dbccompany.vemserfinal.DTO;

import br.com.dbccompany.vemserfinal.Entity.*;
import br.com.dbccompany.vemserfinal.Service.EspacosService;

import java.util.List;

public class ContratacaoDTO {
    private Integer id;
    private ClientesEntity cliente;
    private EspacosEntity espaco;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private double desconto = 0.0;
    private int prazo;
    private List<PagamentosEntity> pagamentos;
    private double valorContratado;

    public ContratacaoDTO() {}

    public ContratacaoDTO(ContratacaoEntity contratacao) {
        this.id = contratacao.getId();
        this.cliente = contratacao.getCliente();
        this.espaco = contratacao.getEspaco();
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.pagamentos = contratacao.getPagamentos();
        this.valorContratado = contratacao.getEspaco().getValor() - this.desconto;
    }

    public ContratacaoEntity convert() {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setId(this.id);
        contratacao.setCliente(this.cliente);
        contratacao.setEspaco(this.espaco);
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setPagamentos(this.pagamentos);
        return contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public double getValorContratado() {
        return valorContratado;
    }

    public void setValorContratado(double valorCobrado) {
        this.valorContratado = valorCobrado;
    }
}
