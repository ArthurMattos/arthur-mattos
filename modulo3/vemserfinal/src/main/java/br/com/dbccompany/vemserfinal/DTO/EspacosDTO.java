package br.com.dbccompany.vemserfinal.DTO;

import br.com.dbccompany.vemserfinal.Entity.ContratacaoEntity;
import br.com.dbccompany.vemserfinal.Entity.EspacosEntity;
import br.com.dbccompany.vemserfinal.Entity.EspacosPacotesEntity;
import br.com.dbccompany.vemserfinal.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemserfinal.Util.TratarValorUtil;

import java.util.List;

public class EspacosDTO {

    private Integer id;
    private String nome;
    private int qtdPessoas;
    private String valor;
    private List<SaldoClienteEntity> saldosClientes;
    private List<EspacosPacotesEntity> espacosPacotes;
    private List<ContratacaoEntity> contratacao;

    public EspacosDTO() {}

    public EspacosDTO(EspacosEntity espaco) {
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        this.valor = TratarValorUtil.trataValorSaida(espaco.getValor());
        this.saldosClientes = espaco.getSaldosClientes();
        this.espacosPacotes = espaco.getEspacosPacotes();
        this.contratacao = espaco.getContratacao();
    }

    public EspacosEntity convert() {
        EspacosEntity espaco = new EspacosEntity();
        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        espaco.setValor(TratarValorUtil.trataValorEntrada(this.valor));
        espaco.setSaldosClientes(this.saldosClientes);
        espaco.setEspacosPacotes(this.espacosPacotes);
        espaco.setContratacao(this.contratacao);
        return espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}