package br.com.dbccompany.vemserfinal.DTO;

import br.com.dbccompany.vemserfinal.Entity.EspacosEntity;
import br.com.dbccompany.vemserfinal.Entity.EspacosPacotesEntity;
import br.com.dbccompany.vemserfinal.Entity.PacotesEntity;
import br.com.dbccompany.vemserfinal.Entity.TipoContratacaoEnum;
import br.com.dbccompany.vemserfinal.Util.ConvertePrazoParaDiasUtil;

public class EspacosPacotesDTO {

    private Integer id;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private double prazo;
    private EspacosEntity espaco;
    private PacotesEntity pacote;

    public EspacosPacotesDTO() {}

    public EspacosPacotesDTO(EspacosPacotesEntity espacoPacote) {
        this.id = espacoPacote.getId();
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
        this.espaco = espacoPacote.getEspacos();
        this.pacote = espacoPacote.getPacote();
    }

    public EspacosPacotesEntity convert() {
        this.prazo = ConvertePrazoParaDiasUtil.converteParaDias(quantidade, tipoContratacao);
        EspacosPacotesEntity espacoPacote = new EspacosPacotesEntity();
        espacoPacote.setId(this.id);
        espacoPacote.setTipoContratacao(this.tipoContratacao);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        espacoPacote.setEspacos(this.espaco);
        espacoPacote.setPacote(this.pacote);
        return espacoPacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPrazo() {
        return prazo;
    }

    public void setPrazo(double prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }
}

