package br.com.dbccompany.vemserfinal.DTO;

import br.com.dbccompany.vemserfinal.Entity.ClientesPacotesEntity;
import br.com.dbccompany.vemserfinal.Entity.EspacosPacotesEntity;
import br.com.dbccompany.vemserfinal.Entity.PacotesEntity;
import br.com.dbccompany.vemserfinal.Util.TratarValorUtil;

import java.util.List;

public class PacotesDTO {

    private Integer id;
    private List<EspacosPacotesEntity> espacosPacotes;
    private String valor;
    private List<ClientesPacotesEntity> clientesPacotes;

    public PacotesDTO() {}

    public PacotesDTO(PacotesEntity pacote) {
        this.id = pacote.getId();
        this.espacosPacotes = pacote.getEspacosPacotes();
        this.valor = TratarValorUtil.trataValorSaida(pacote.getValor());
        this.clientesPacotes = pacote.getClientesPacotes();
    }

    public PacotesEntity convert() {
        PacotesEntity pacote = new PacotesEntity();
        pacote.setId(this.id);
        pacote.setEspacosPacotes(this.espacosPacotes);
        pacote.setValor(TratarValorUtil.trataValorEntrada(this.valor));
        pacote.setClientesPacotes(this.clientesPacotes);
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}