package br.com.dbccompany.vemserfinal.DTO;

import br.com.dbccompany.vemserfinal.Entity.*;

import java.time.LocalDate;
import java.util.List;

public class SaldoClienteDTO {

    private SaldoClienteEntityId id;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private LocalDate vencimento;
    private ClientesEntity cliente;
    private EspacosEntity espaco;
    private List<AcessosEntity> acesso;

    public SaldoClienteDTO() {}

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente) {
        this.id = saldoCliente.getId();
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
        this.cliente = saldoCliente.getCliente();
        this.espaco = saldoCliente.getEspaco();
        this.acesso = saldoCliente.getAcesso();
    }

    public SaldoClienteEntity convert() {
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(this.id);
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        saldoCliente.setCliente(this.cliente);
        saldoCliente.setEspaco(this.espaco);
        saldoCliente.setAcesso(this.acesso);
        return saldoCliente;
    }

    public SaldoClienteEntityId getId() {
        return id;
    }

    public void setId(SaldoClienteEntityId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessosEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessosEntity> acesso) {
        this.acesso = acesso;
    }
}
