package br.com.dbccompany.vemserfinal.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class AcessosEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue( generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private boolean is_entrada;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate data;
    private boolean is_excecao;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns(value = {
            @JoinColumn(name = "ID_ESPACO_SALDO_CLIENTE", nullable = false),
            @JoinColumn(name = "ID_CLIENTE_SALDO_CLIENTE", nullable = false)
    })
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }


    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isIs_entrada() {
        return is_entrada;
    }

    public void setIs_entrada(boolean is_entrada) {
        this.is_entrada = is_entrada;
    }

    public boolean isIs_excecao() {
        return is_excecao;
    }

    public void setIs_excecao(boolean is_excecao) {
        this.is_excecao = is_excecao;
    }
}
