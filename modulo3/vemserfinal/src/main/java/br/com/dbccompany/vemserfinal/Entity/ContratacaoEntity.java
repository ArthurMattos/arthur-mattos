package br.com.dbccompany.vemserfinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(nullable = false)
    private int quantidade;
    @Column(nullable = true)
    private double desconto;
    @Column(nullable = false)
    private int prazo;

    @Enumerated(EnumType.STRING)
    @Column (nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_ESPACOS")
    private EspacosEntity espacos;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_CLIENTES" )
    private ClientesEntity clientes;

    @OneToMany (mappedBy = "contratacao")
    private List<PagamentosEntity> pagamentos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return clientes;
    }

    public void setCliente(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public EspacosEntity getEspaco() {
        return espacos;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espacos = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        if(quantidade > 0) {
            this.quantidade = quantidade;
        }
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        if(prazo > 0) {
            this.prazo = prazo;
        }
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

}