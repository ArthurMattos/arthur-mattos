package br.com.dbccompany.vemserfinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacosEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(nullable = false, unique = true)
    private String nome;
    @Column(nullable = false)
    private int qtdPessoas;
    @Column(nullable = false)
    private double valor;

    @OneToMany(mappedBy = "espacos")
    private List<SaldoClienteEntity> saldoCliente;

    @OneToMany(mappedBy = "espacos")
    private List<EspacosPacotesEntity> espacosPacotes;

    @OneToMany(mappedBy = "espacos")
    private List<ContratacaoEntity> contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldoCliente;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldoCliente = saldosClientes;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
