package br.com.dbccompany.vemserfinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacosPacotesEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ESPACOS_PACOTES_SEQ", sequenceName = "ESPACOS_PACOTES_SEQ")
    @GeneratedValue( generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    //recebe id de pacotess
    private int quantidade;
    private double prazo;

    @Enumerated(EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO", nullable = false)
    private EspacosEntity espacos;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTE", nullable = false)
    private PacotesEntity pacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPrazo() {
        return prazo;
    }

    public void setPrazo(double prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public PacotesEntity getPacote() {
        return pacotes;
    }

    public void setPacote(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }
}
