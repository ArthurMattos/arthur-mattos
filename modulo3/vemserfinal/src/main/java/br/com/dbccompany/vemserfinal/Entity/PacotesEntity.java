package br.com.dbccompany.vemserfinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacotesEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private double valor;

    @OneToMany(mappedBy = "pacotes")
    private List<EspacosPacotesEntity> espacosPacotes;

    @OneToMany(mappedBy = "pacotes")
    private List<ClientesPacotesEntity> clientesPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
