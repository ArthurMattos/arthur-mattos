package br.com.dbccompany.vemserfinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PagamentosEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE_PACOTE", nullable = false)
    private ClientesPacotesEntity clientesPacotes;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRATACAO", nullable = false)
    private ContratacaoEntity contratacao;

    @Enumerated(EnumType.STRING)
    private TipoPagamentoEnum tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public ClientesPacotesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}

