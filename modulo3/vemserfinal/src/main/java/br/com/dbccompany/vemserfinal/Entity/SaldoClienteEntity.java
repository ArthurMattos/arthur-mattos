package br.com.dbccompany.vemserfinal.Entity;

import br.com.dbccompany.vemserfinal.Entity.*;
import br.com.dbccompany.vemserfinal.Entity.SaldoClienteEntityId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ClientesEntity.class)
public class SaldoClienteEntity extends EntityAbstract<SaldoClienteEntityId> {

    @EmbeddedId
    private SaldoClienteEntityId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate vencimento;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE", nullable = false, insertable = false, updatable = false)
    private ClientesEntity clientes;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO", nullable = false, insertable = false, updatable = false)
    private EspacosEntity espacos;

    @OneToMany( mappedBy = "saldoCliente")
    private List<AcessosEntity> acesso;

    public SaldoClienteEntityId getId() {
        return id;
    }

    public void setId(SaldoClienteEntityId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getCliente() {
        return clientes;
    }

    public void setCliente(ClientesEntity cliente) {
        this.clientes = cliente;
    }

    public EspacosEntity getEspaco() {
        return espacos;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espacos = espaco;
    }

    public List<AcessosEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessosEntity> acesso) {
        this.acesso = acesso;
    }
}
