package br.com.dbccompany.vemserfinal.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SaldoClienteEntityId implements Serializable {

    @Column(name = "ID_CLIENTES")
    private int idClientes;

    @Column(name = "ID_ESPACOS")
    private int idEspacos;

    public SaldoClienteEntityId() {}

    public SaldoClienteEntityId(int idEspacos, int idClientes) {
        this.idEspacos = idEspacos;
        this.idClientes = idClientes;
    }

    public int getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(int idClientes) {
        this.idClientes = idClientes;
    }

    public int getIdEspacos() {
        return idEspacos;
    }

    public void setIdEspacos(int idEspacos) {
        this.idEspacos = idEspacos;
    }
}
