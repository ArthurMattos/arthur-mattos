package br.com.dbccompany.vemserfinal.Entity;

public enum TipoContratacaoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
