package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.AcessosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {

    List<AcessosEntity> findAll ();
    AcessosEntity findById(int id);

}

