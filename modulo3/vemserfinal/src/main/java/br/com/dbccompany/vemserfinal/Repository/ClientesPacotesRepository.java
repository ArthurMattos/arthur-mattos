package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.ClientesPacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotesEntity, Integer> {

    List<ClientesPacotesEntity> findAll ();
    ClientesPacotesEntity findById(int id);

}
