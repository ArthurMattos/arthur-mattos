package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {

    List<ContatoEntity> findAll ();
    ContatoEntity findById(int id);

}

