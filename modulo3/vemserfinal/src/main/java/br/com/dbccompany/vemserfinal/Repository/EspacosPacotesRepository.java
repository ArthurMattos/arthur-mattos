package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.EspacosPacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotesEntity, Integer> {

    List<EspacosPacotesEntity> findAll ();
    EspacosPacotesEntity findById(int id);

}
