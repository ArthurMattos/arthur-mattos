package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.EspacosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EspacosRepository extends CrudRepository<EspacosEntity, Integer> {

    EspacosEntity findByNome(String nome);
    List<EspacosEntity> findAllByNome(String nome);
    List<EspacosEntity> findAll ();
    EspacosEntity findById(int id);

}
