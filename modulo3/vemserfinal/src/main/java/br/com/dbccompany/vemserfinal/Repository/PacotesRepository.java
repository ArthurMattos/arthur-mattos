package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.PacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PacotesRepository extends CrudRepository<PacotesEntity, Integer> {

    List<PacotesEntity> findAll ();
    PacotesEntity findById(int id);

}
