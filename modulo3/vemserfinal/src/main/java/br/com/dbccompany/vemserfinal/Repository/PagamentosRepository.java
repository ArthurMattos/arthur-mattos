package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.PagamentosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Integer> {

    List<PagamentosEntity> findAll ();
    PagamentosEntity findById(int id);

}
