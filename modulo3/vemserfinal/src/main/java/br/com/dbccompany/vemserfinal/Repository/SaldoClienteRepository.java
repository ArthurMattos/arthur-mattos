package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemserfinal.Entity.SaldoClienteEntityId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteEntityId> {
}