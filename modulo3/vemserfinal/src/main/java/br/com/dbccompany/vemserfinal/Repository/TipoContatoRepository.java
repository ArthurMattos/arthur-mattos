package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {

    TipoContatoEntity findByNome(String nome);
    List<TipoContatoEntity> findAllByNome(String nome);
    List<TipoContatoEntity> findAll ();
    TipoContatoEntity findById(int id);

}
