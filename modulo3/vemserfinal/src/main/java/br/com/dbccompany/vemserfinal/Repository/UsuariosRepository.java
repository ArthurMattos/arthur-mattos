package br.com.dbccompany.vemserfinal.Repository;

import br.com.dbccompany.vemserfinal.Entity.UsuariosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UsuariosRepository extends CrudRepository<UsuariosEntity, Integer> {

    UsuariosEntity findByNome(String nome);
    List<UsuariosEntity> findAllByNome(String nome);
    List<UsuariosEntity> findAll ();
    UsuariosEntity findById(int id);
    UsuariosEntity findByLogin(String login);
}
