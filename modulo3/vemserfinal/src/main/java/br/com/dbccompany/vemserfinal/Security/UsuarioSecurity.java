package br.com.dbccompany.vemserfinal.Security;

import br.com.dbccompany.vemserfinal.Entity.UsuariosEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UsuarioSecurity implements UserDetails {

    private UsuariosEntity usuarioEntity;
    private String username;
    private String password;

    public UsuarioSecurity() {};

    public UsuarioSecurity(UsuariosEntity usuarioEntity) {
        this.usuarioEntity = usuarioEntity;
        username = usuarioEntity.getLogin();
        password = usuarioEntity.getSenha();
    };

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

