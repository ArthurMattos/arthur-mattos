package br.com.dbccompany.vemserfinal.Security;

import br.com.dbccompany.vemserfinal.Entity.UsuariosEntity;
import br.com.dbccompany.vemserfinal.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UsuarioSecurityService implements UserDetailsService {

    @Autowired
    private UsuariosRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UsuariosEntity usuarioEntity = usuarioRepository.findByNome(username);
        if (usuarioEntity == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UsuarioSecurity(usuarioEntity);
    }
}