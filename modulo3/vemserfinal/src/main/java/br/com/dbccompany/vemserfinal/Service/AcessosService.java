package br.com.dbccompany.vemserfinal.Service;

import br.com.dbccompany.vemserfinal.VemserfinalApplication;
import br.com.dbccompany.vemserfinal.DTO.AcessosDTO;
import br.com.dbccompany.vemserfinal.Entity.AcessosEntity;
import br.com.dbccompany.vemserfinal.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemserfinal.Entity.SaldoClienteEntityId;
import br.com.dbccompany.vemserfinal.Repository.AcessosRepository;
import br.com.dbccompany.vemserfinal.Repository.SaldoClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessosService extends ServiceAbstract<AcessosRepository, AcessosEntity, Integer>{

    private Logger logger = LoggerFactory.getLogger(VemserfinalApplication.class);

    @Autowired
    SaldoClienteRepository saldoRepository;

    public AcessosDTO salvarEntrada( AcessosDTO acesso ){

            if( acesso.isEntrada() ) {
                SaldoClienteEntityId id = acesso.getSaldoCliente().getId();
                SaldoClienteEntity saldo = saldoRepository.findById(id).get();
                if(saldo.getQuantidade() <= 0) {
                    return null;
                }
            }
            AcessosEntity acessoEntity = acesso.convert();
            AcessosDTO newDto = new AcessosDTO(super.salvar(acessoEntity));
            return newDto;
        }


}