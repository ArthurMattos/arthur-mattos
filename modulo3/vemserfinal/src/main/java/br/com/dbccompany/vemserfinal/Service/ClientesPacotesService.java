package br.com.dbccompany.vemserfinal.Service;

import br.com.dbccompany.vemserfinal.Entity.ClientesPacotesEntity;
import br.com.dbccompany.vemserfinal.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesPacotesService extends ServiceAbstract<ClientesPacotesRepository, ClientesPacotesEntity, Integer>{

}