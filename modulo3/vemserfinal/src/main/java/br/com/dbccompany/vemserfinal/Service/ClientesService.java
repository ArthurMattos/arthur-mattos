package br.com.dbccompany.vemserfinal.Service;


import br.com.dbccompany.vemserfinal.Entity.ClientesEntity;
import br.com.dbccompany.vemserfinal.Repository.ClientesRepository;
import org.springframework.stereotype.Service;


@Service
public class ClientesService extends ServiceAbstract<ClientesRepository, ClientesEntity, Integer> {

}