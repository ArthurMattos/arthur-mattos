package br.com.dbccompany.vemserfinal.Service;

import br.com.dbccompany.vemserfinal.VemserfinalApplication;
import br.com.dbccompany.vemserfinal.Entity.EntityAbstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAbstract<R extends CrudRepository<E, T>, E extends EntityAbstract, T> {

    private Logger logger = LoggerFactory.getLogger(VemserfinalApplication.class);

    @Autowired
    R repository;

    @Transactional(rollbackFor = Exception.class)
    public E salvar(E entidade) {
        try {
            return repository.save(entidade);
        } catch( Exception e ) {
            logger.error("Erro ao salvar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public E editar(E entidade, T id) {
        try {
            return repository.save(entidade);
        } catch ( Exception e ) {
            logger.error("Erro ao editar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public List<E> todos() {
        try {
            return (List<E>) repository.findAll();
        } catch ( Exception e ) {
            logger.error("Erro ao ver todas as entidades: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public E porId(T id) {
        try {
            return (E) repository.findById(id).get();
        } catch ( Exception e ) {
            logger.error("Erro ao ver entidade por id: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
