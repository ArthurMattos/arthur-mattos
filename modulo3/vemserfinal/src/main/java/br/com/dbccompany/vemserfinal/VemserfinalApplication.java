package br.com.dbccompany.vemserfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VemserfinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(VemserfinalApplication.class, args);
	}

}
