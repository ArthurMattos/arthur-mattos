import React, { useEffect, useState } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import '../../containers/home/Home.css';
import ProdutosApi from '../../models/produtosApi';

export default function DemoCarousel() {
  const produtosApi = new ProdutosApi();
  const [banner, setBanner] = useState([]);

  useEffect(() => {
    produtosApi.buscarBanners().then( res => setBanner(res));
  }, [])

  return (
    <Carousel  className="carousel">
      { banner && (banner.map(ban => {
        return (
          <div>
            <img src={ban.imagem} alt="banner"/>
          </div>
        )
      }))}
    </Carousel>
  );
}