import React from 'react';

import '../../containers/home/Home.css';

export default function Button(props) {
  return (
  <button className={props.classe} type={props.tipo} >{props.nome}</button>
  )
}