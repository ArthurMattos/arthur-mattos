import React from 'react';

const CampoBusca = ( { placeholder, metodo } ) =>
  <React.Fragment>
    <input type="text" placeholder={ placeholder } onKeyPress={ metodo } />
  </React.Fragment>

export default CampoBusca;