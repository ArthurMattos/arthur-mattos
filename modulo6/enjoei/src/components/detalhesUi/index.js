import React from 'react';
import Rock from '../../img/hand.png'
import Smiley from '../../img/smiley.png'

import '../../containers/home/Home.css';
import Button from '../buttonUi';

function dataFormatada( criacao ) {
  let data = criacao.split('-');
  let meses = ["jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago",
    "set", "out", "nov", "dez"];
  let dataFormatada = `${meses[data[2] - 1]}/${data[0]}`;
  return dataFormatada;
}

const DetalhesUi = ( { produto, detalhes, vendedor} ) => 
  <div className="lista">
    {
      produto && 
      detalhes &&
      vendedor &&
        <>
        <div className='container'>
            <div className='container-left'>
                <img src={produto.imagem} alt={produto.nome}/>
            </div>
            <div className='container-middle'>
                <img src={ Rock } className="icon"alt="question"></img> 
                <img src={ Smiley } className="icon"alt="question"></img> 
            </div>
            <div className='container-right'>
            <div className='nome'>{produto.nome}</div>
            <div className='marca'>{detalhes.marca}<span>seguir marca</span></div>
            <div className='preco'>R${produto.preco}</div>
            <div className='botoes'>
              <Button nome={'eu quero'} classe={'rosa'} />
              <Button nome={'adicionar à sacolinha'} classe={'branco'} />
              <Button nome={'fazer oferta'} classe={'branco'} />
            </div>
            <div className='detalhes'>
            <div className='tamanho'>
            <div className=''>tamanho</div>
            <div className=''>-</div>
            </div>
            <div className='quadrado'>
            <div className='marca-quadrado'>{detalhes.marca}</div>
            <div className='condicao'>{detalhes.condicao}</div>
            <div className='codigo'>{detalhes.codigo}</div>
            </div>
            </div>
            <div className="avaliacao">
             <div className="table-up">
               <span>{vendedor.nome}</span>
             </div>
            <div className="table-middle">
              <div>
                <span>cidade</span>
                <p>{ vendedor.cidade }</p>
              </div>
            <div className="">
              <span>estado</span>
              <p>{ vendedor.estado }</p>
            </div>
            <div className="">
              <span>tempo médio de envio</span>
              <p>{vendedor.tempoMedio === '' ? 'N/D' : vendedor.tempoMedio}</p>
            </div>
            </div>
            <div className="table-bottom">
              <div className="">
                <span>à venda</span>
                <p> 77</p>
              </div>
            <div className="">
              <span>vendidos</span>
              <p>{ vendedor.qtdItensVendidos }</p>
            </div>
            <div className="">
              <span>no enjoei desde</span>
              <p>{ vendedor.criacao && (dataFormatada( vendedor.criacao )) }</p>
            </div>
          </div>
        </div>
            </div>
      </div>
      </>
      
    }
  </div>

    export default DetalhesUi;