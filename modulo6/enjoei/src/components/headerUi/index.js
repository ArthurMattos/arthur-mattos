import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../img/logo.png';

import '../../containers/home/Home.css';

export default function Header() {
  return(
    <header className="header">
      <div className="img">
        <Link to="/"><img src={ Logo } className="imagem-menu" alt="logo"/></Link>
      </div>
    </header>
  )
}