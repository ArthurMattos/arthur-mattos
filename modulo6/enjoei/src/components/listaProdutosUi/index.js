import React from 'react';
import { Link } from 'react-router-dom'

import '../../containers/home/Home.css';

 const ListaProdutosUi = ( { listaProdutos, categoria } ) =>
  
  <div className="lista">
    {
      categoria && categoria.map(c =>
        <>
        <div className='categoria'>
        <p className='titulo'>{c.nome}</p>
        <p className='sub'>{c.subTitulo}</p>
        </div>
        {
          listaProdutos._todos.map( e => {
            if( c.id === e.idCategoria ) {
              return (
                <div className="produto">
                  <div className="item">  
                    <Link to={ `/produto/${ e.id }` } ><img  className='imagem' src={e.imagem} alt={e.nome}/></Link>
                    <p className='preco-menu'>R${ e.preco }</p>
                  </div>
               </div>
              )
            }
          })
        }
        </>
      )
      }
    </div>

export default ListaProdutosUi;