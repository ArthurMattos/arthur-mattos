import React from 'react';
import Logo from '../../img/logo.png';
import Question from '../../img/question.png'
import Search from '../../img/loupe.png'
import CampoBusca from '../../components/campoBusca'
import Button from '../../components/buttonUi';
import { Link } from 'react-router-dom'; 


import "../../containers/home/Home.css";

export default function Menu() {
  return (
    <div className="menu">
      <div className="box">
      <Link to="/"><img src={ Logo } className="imagem-menu" alt="logo"/></Link>
      <div className="pesquisar">
      <CampoBusca placeholder='busque "ellus"'/>
      <img src={ Search } className="icon"alt="search"></img>
      </div>
      </div>
      <div className="nav">
        <ul>
          <Button classe={ 'entrar' }nome={'moças'}/>
          <Button classe={ 'entrar' }nome={'rapazes'}/>
          <Button classe={ 'entrar' }nome={'kids'}/>
          <Button classe={ 'entrar' }nome={'casa&tal'}/>
          <img src={ Question } className="icon"alt="question"></img>   
        </ul>
      </div>
      <div className="botoes">
         <Link to="/login"><Button classe={ 'entrar' }nome={'entrar'}/></Link>
        <Button classe={'rosa'} nome={'quero vender'}/>
      </div>
    </div>
  )
}