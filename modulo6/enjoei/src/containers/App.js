import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { Home, Detalhes, Login, Cadastro } from '../containers'
import { PrivateRoute } from '../components/rotaPrivada'

export default class App extends Component {
  
  render(){
    return (
      <div className="App">
      <Router>
        <Route path="/" exact component={ Home } />
        <PrivateRoute path="/produto/:id" exact component={ Detalhes } />
        <Route path="/login" exact component={ Login } />
        <Route path="/cadastro" exact component={ Cadastro } />
      </Router>
      </div>
    )
  };
}