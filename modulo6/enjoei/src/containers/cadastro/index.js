import React, { useState } from 'react';
import Header from '../../components/headerUi';
import ProdutosApi from '../../models/produtosApi';

import '../home/Home.css';

function Cadastro(props) {
  const [ user, setUser ] = useState('');
  const [ login, setLogin ] = useState('');
  const [ senha, setSenha ] = useState('');

  const handler = (e) => {
    e.preventDefault();
    const produtos = new ProdutosApi();
    produtos.cadastrarUsuario( { nome:user, login: login, senha: senha } )
  }

  return (
    <React.Fragment>
      <Header/>
      <form onSubmit={ handler }>
            <label>Nome</label>
            <input type='text' name='nome' onChange={ ev => setUser(ev.target.value) } />
            <label>Login</label>
            <input type='email' name='login' onChange={ ev => setLogin(ev.target.value) } />
            <label>Senha</label>
            <input type='password' name='senha' onChange={ ev => setSenha(ev.target.value) } />
          <button type='submit'  >Salvar</button>
      </form>
    </React.Fragment>
  )
}

export default Cadastro;