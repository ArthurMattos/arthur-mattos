import React, { Component, Fragment } from 'react';
import { Menu } from '../../components';
import ProdutosApi from '../../models/produtosApi';
import DetalhesUi from '../../components/detalhesUi';
import Desconto from '../../components/desconto';

class Detalhes extends Component {
  constructor(props) {
    super(props);
    this.produtosApi = new ProdutosApi();
    this.state = {
      produto: null,
      detalhes: null,
      vendedor: []
    }
  }

  componentWillMount() {
    const idProduto = this.props.match.params.id;
    const requisicoes = [
      this.produtosApi.buscarProduto(idProduto),
      this.produtosApi.buscarDetalhes(idProduto),
      this.produtosApi.buscarTodosVendedores()
    ]
    
    Promise.all(requisicoes)
    .then(response => {
      this.setState( (state) => {
        return {
          ...state,
          produto: response[0],
          detalhes: response[1],
          vendedor: response[2]
        }
      })
    })
  }

  render() { 
    const { produto, detalhes, vendedor} = this.state;
    return ( 
      <>
      <Desconto/>
      <Menu/>
      {vendedor.length > 0 && (
        vendedor.map( v => {
          if(produto.idVendedor === v.id) {
            return (
            <DetalhesUi produto={ produto } detalhes={ detalhes } vendedor={ v }/>
            )
          }
        })  
      )}
      </>
   );
  }
}
export default Detalhes;