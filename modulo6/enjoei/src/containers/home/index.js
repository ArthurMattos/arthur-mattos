import React, { Component } from 'react';
import { Menu, ListaProdutosUi, Banner, Desconto } from '../../components';
import ProdutosApi from '../../models/produtosApi'
import ListaProdutos from '../../models/listaProdutos'


export default class Home extends Component {
  constructor (props) {
    super(props);
    this.produtosApi = new ProdutosApi();
    this.state = {
      listaProdutos: []
    }
  }

  componentDidMount () {
    const requisicoes = [
      this.produtosApi.buscarTodosCategorias(),
      this.produtosApi.buscar()
    ];

    Promise.all( requisicoes )
    .then ( respostas => {
      const categoriasDoServidor = respostas[0];
      const produtosDoServidor = respostas[1]; 
      this.categoria = respostas[0]
      const listaProdutosNova = new ListaProdutos(categoriasDoServidor, produtosDoServidor);
      const produto = listaProdutosNova.produtosAleatorios;
      const id = produto.id;
      this.setState((state) => {
        return { ...state, id, listaProdutos: listaProdutosNova };
      });
    })
  }

  render() { 
    const { listaProdutos } = this.state;
    const { categoria } = this;
    return ( 
      <React.Fragment>
        <Desconto/>
        <Menu/>
        <Banner/>
        <ListaProdutosUi listaProdutos={ listaProdutos } categoria={categoria}/>
      </React.Fragment>
    );
  }
}
 