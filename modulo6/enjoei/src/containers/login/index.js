import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Button, Header } from '../../components';
import '../../containers/home/Home.css';
import ProdutosApi from '../../models/produtosApi';


class Login extends Component {
  constructor(props) {
    super(props);
    this.produtosApi = new ProdutosApi();
    this.state = {
      user:'',
      login:'',
      senha:''
    };
  }

  handler = evt => {
    evt.preventDefault();
    const produtosApi = new ProdutosApi();
    produtosApi.buscarUsuario( this.login, this.senha)
      .then( resposta => {
        this.setState( state => {
          return {
          ...state,
          user: resposta,
          login: resposta.login,
          senha: resposta.senha
        }
        })
      })
    
    if( this.state.user ) {
      localStorage.setItem('user', 'ativo')
      this.props.history.push('/');
    }
  }

  pegarLogin(evt){
    evt.preventDefault();
    this.setState( state => {
      return {
        ...state,
        login: evt.target.value
      }
    })
  }

  pegarSenha(evt){
    evt.preventDefault();
    this.setState( state => {
      return {
        ...state,
        senha: evt.target.value
      }
    })
  }

  render() {
    return (
      <Fragment>
        <Header/>
        <section className="body">
          <div className="login">
            <h1>faça login no enjoei</h1>
            <div className="cadastro">
              <Button classe={'azul'} nome={'entre usando o facebook'} />
              <span>ou</span>
              <form onSubmit={this.handler} className="campo">
                <label htmlFor="">email</label>
                <input type="text" onBlur={evt => this.pegarLogin(evt)}/>
                <label htmlFor="">senha super secreta</label>
                <input type="text" onBlur={evt => this.pegarSenha(evt)}/>
                <div className="check-senha">
                  <div className="checkbox">
                    <input type="checkbox" />
                    <label htmlFor=""> continuar conectado</label>
                  </div>
                  <Link className='link'>esqueci a senha</Link>
                </div>
                <Button tipo={'submit'} classe={'rosa'} nome={'entrar'} />
              </form>
              <Link to='/cadastro' className='link'>não tenho conta</Link>
            </div>
          </div>
        </section>
      </Fragment>
    )
  }
}

export default Login;