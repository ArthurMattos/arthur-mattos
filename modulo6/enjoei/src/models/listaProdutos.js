import Produto from "./produto";
import Categoria from './categoria'

function _sortear( min, max ){
  min = Math.ceil(min);
  max = Math.floor( max );
  return Math.floor( Math.random() *  (max - min ) ) + min;
}

export default class ListaProdutos {

  constructor( categoriasDoServidor = [], produtosDoServidor = [] ) {
      this._todos = produtosDoServidor.map( elem => new Produto( elem.id, elem.idVendedor, elem.idCategoria, elem.nome, elem.preco, elem.imagem ) );
      this.categorias = categoriasDoServidor.map ( elem => new Categoria( elem.id, elem.nome, elem.subTitulo) );
    }

    get produtosAleatorios() {
      const indice = _sortear( 0, this._todos.length );
      return this._todos[ indice ];
    }
}