import axios from 'axios';

const address = 'http://localhost:9000/api/';
const _get = address => new Promise( (resolve, reject ) => axios.get( address ).then( response => resolve(response.data) ) );
const _post = ( address, dados ) => new Promise( (resolve, reject ) => axios.post( address, dados ).then( response => resolve(response.data) ) );

export default class ProdutosApi {
  
  async buscar() {
    return await _get( `${ address }produtos` );
  }
  
  async buscarTodosDetalhes() {
    return await _get( `${ address }detalhes` );
  }

  async buscarTodosComentarios() {
    return await _get( `${ address }comentarios` );
  }

  async buscarTodosComentariosdeUmProduto( id ) {
    return await _get( `${ address }comentarios?idProduto=${ id }` );
  }

  async buscarTodosCategorias() {
    return await _get( `${ address }categorias` );
  }
  
  async buscarProduto( id ) {
    const response = await _get( `${ address }produtos?id=${ id }` );
    return response[ 0 ];
  }

  async buscarDetalhes( id ) {
    const response = await _get( `${ address }detalhes?idProduto=${ id }` );
    return response[ 0 ];
  }

  async registrarComentario( { idProduto, nomeProduto, codigoProduto, nomeUser, idVendedor, comentario  } ) {
    const response = await _post( `${ address }comentarios`, { idProduto, nomeProduto, codigoProduto, nomeUser, idVendedor, comentario  } );
    return response[ 0 ];
  }

  async filtrarPorCategoria ( idCategoria ) {
    const response =  _get( `${ address }produtos?idCategoria=${ idCategoria }`);
    return response[ 0 ];
  }

  async cadastrarUsuario( { nome, login, senha } ) {
    const response = await _post( `${ address }cadastro`, { nome, login, senha } );
    return response[ 0 ];
  }

  async buscarVendedor ( id ) {
    const response = await _get(`${address}vendedor?id=${id}`);
    return response[ 0 ];
  }

  async buscarTodosVendedores () {
    const response = await _get(`${address}vendedor`);
    return response;
  }

  async buscarUsuario (login, senha) {
    const response = await _post(`${address}cadastro`, {}, {auth:{ login:login, senha:senha }})
    return response;
  }

  async buscarBanners() {
    return await _get( `${ address }banners` );
  }
}