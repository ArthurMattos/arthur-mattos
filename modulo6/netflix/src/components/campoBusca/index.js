import React from 'react';

const CampoBusca = ( { placeholder, atualizaValor } ) =>
  <React.Fragment>
    <input type="text" placeholder={ placeholder } onKeyPress={ atualizaValor } />
  </React.Fragment>

export default CampoBusca;