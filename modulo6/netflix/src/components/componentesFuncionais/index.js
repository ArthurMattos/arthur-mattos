/*
Botão episódio aleatório e botão avaliar:

<Lista
    classeName="botoes"
    dados={ [
        { cor: "vermelho", nome: "Próximo", metodo: this.sortear.bind( this ) },
        { cor: "amarelo", nome: "Assistir", metodo: this.marcarComoAssistido }
    ] }
    funcao={ ( item, i ) => this.linha( item, i ) } />
<MeuInputNumero
        className="descricao-nota"
        placeholder="Nota de 1 a 5"
        mensagem="Qual a sua nota para esse episódio?"
        obrigatorio={ true }
        visivel={ episodios.assistido || false }
        deveExibirErro={ deveExibirErro }
        atualizarValor={ this.salvarNota.bind( this ) } />

Botão Lista de Notas:

<ButtonUi link={{pathname: "/lista", state: { listaEpisodios } }} nome="Lista de notas"/>

Detalhes do Episódio:

import React, { Component } from 'react';
import  { EpisodiosApi, Episodios } from '../../models/';
import EpisodioUi from '../../components/episodioUi'

export default class DetalhesEpisodios extends Component {

    constructor ( props ) {
        super( props );
        this.episodiosApi = new EpisodiosApi
        this.state = {
            detalhes: null
        }
    } 

    componentDidMount () {
        const episodioId = this.props.match.params.id;
        const requisicoes = [
            this.episodiosApi.buscarEpisodio ( episodioId ),
            this.episodiosApi.buscarDetalhes ( episodioId ),
            this.episodiosApi.buscarNota ( episodioId )
        ];

        Promise.all( requisicoes )
        .then( resposta => {
            const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl } = resposta[0];
            this.setState({
                episodios: new Episodios(id, nome, duracao, temporada, ordemEpisodio, thumbUrl),
                detalhes: resposta[1],
                objNota: resposta[2]
            })
        })
    }

    render () {
        const { episodios, detalhes, objNota } = this.state;

        return (
            <React.Fragment>
                <header className="App-header">
                { episodios && ( <EpisodioUi episodios={ episodios } /> )}
                {
                    detalhes ?
                    <React.Fragment>
                        <p> { detalhes.sinopse } </p>
                        <span> { new Date( detalhes.dataEstreia ).toLocaleDateString() } </span>
                        <span>IMDB: { detalhes.notaImdb * 0.5 } </span>
                        <span>Sua nota: { objNota ? objNota.nota : 'N/D' } </span>
                    </React.Fragment> : null
                }
                </header>
            </React.Fragment>
        )
    }
}

*/