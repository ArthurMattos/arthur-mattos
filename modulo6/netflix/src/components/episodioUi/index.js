import React, { Component } from 'react';
import '../../containers/home/Home.scss';

export default class EpisodioUi extends Component {
  render(){
    const { episodios } = this.props;
    
    return (
      <React.Fragment>
        <div className='detalhes'>
        <img className='imagem-episodio' src={ episodios.thumbUrl } alt={ episodios.nome } />
        <li className='lista-episodios lista-episodios-nome'>{ episodios.nome }</li>
        <li className='lista-episodios'>Já Assisti: { episodios.assistido ? 'Sim' : 'Não' }, { episodios.qtdAssistido } Vez(es)</li>
        <li className='lista-episodios'>Duração: { episodios.duracaoEmMin }</li>
        <li className='lista-episodios'>Temporada / Episódio: { episodios.temporadaEpisodio }</li>
        <li className='lista-episodios'>Nota: {episodios.nota} </li>
        </div>
      </React.Fragment>
    )
  }
}