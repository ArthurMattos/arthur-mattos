import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ButtonUi from '../buttonUi'
import EpisodiosApi from '../../models/episodiosApi'
import ListaEpisodios from '../../models/listaEpisodios'

import CampoBusca from '../campoBusca';
import '../../containers/home/Home.scss'
import Logo from '../../img/logo.png';

export default class Header extends Component {
    constructor (props) {
        super(props);
        this.episodiosApi = new EpisodiosApi();
        this.state = {
            listaEpisodios: []
        }
    } 

    componentDidMount () {
        this.episodiosApi.buscar().then(resposta => {
            const lista = new ListaEpisodios(resposta); 
            const episodios = lista.episodiosAleatorios;
            const id = episodios.id;
            this.setState(state => {
                return { ...state, id }
            })
        }) 
    }
   
    render() {
        const { metodo } = this.props;

        return (

            <React.Fragment>
                <header className="header">
                    <div class="container">
                        <div class="row">
                            <div class="col col-sm-12">
                                <div className="netflixLogo">
                                    <a id="logo">
                                    <Link to='/'><img className='logo' src={Logo} alt='Logo' /></Link>
                                    </a>
                                </div>
                                <div className='botoes'>
                                    <ButtonUi classe='btn' nome="Assistir aleatório" link={ `/episodios/${this.state.id}` }/>
                                </div>
                                <nav className="main-nav">
                                    <CampoBusca atualizaValor={ metodo } placeholder="Buscar" />
                                </nav>
                            </div>
                        </div>
                    </div>
                </header>
            </React.Fragment>
        )
    }
}