import React from 'react';
import { Link } from 'react-router-dom';

import '../../containers/home/Home.scss'

import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const ListaEpisodiosUi = ( { listaEpisodios } ) => 

<div className='body'>
    {
      listaEpisodios && listaEpisodios.map( e => 
        <React.Fragment key={ e.id }>
          <div className='cards'>
            <Link to={ `episodios/${ e.id }` } className='link'>
              <li className='lista-episodios'>
                <img className='imagem-episodio' src={ e.thumbUrl } alt={ `${ e.nome }` }/>
              </li>
              <li className='lista-episodios lista-episodios-nome'>{ e.nome }</li>
              <li className='lista-episodios'>Duração: { e.duracao }</li>
              <li className='lista-episodios'>Temporada/Episódio: { e.temporada }/{ e.ordem }</li>
              <li className='lista-episodios'>Data de estreia: { e.dataEstreia.toLocaleDateString('pt-br') }</li>
              <li className='lista-episodios lista-episodios-sinopse'>{ e.sinopse }</li>
              <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Nota:</Typography>
                <Rating name="read-only" value={ (e.notaImdb/2 + e.nota) * 0.5 } readOnly precision={0.5}/>
              </Box>
            </Link>
          </div>
        </React.Fragment>
      )
    }
  </div>

export default ListaEpisodiosUi;