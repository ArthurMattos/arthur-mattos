import React, { Component } from 'react';
import  { EpisodiosApi, Episodios } from '../../models/';
import EpisodioUi from '../../components/episodioUi'
import '../../containers/home/Home.scss'
import Header from '../../components/header'

import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export default class DetalhesEpisodios extends Component {

    constructor( props ) {
        super( props );
        this.episodiosApi = new EpisodiosApi();
        this.state = {
          detalhes: null,
        }
      }
    
     componentDidMount() {
        const episodioId = this.props.match.params.id;
        const requisicoes = [
          this.episodiosApi.buscarEpisodio( episodioId ),
          this.episodiosApi.buscarDetalhes( episodioId ),
          this.episodiosApi.buscarNota( episodioId )
        ];
    
        Promise.all( requisicoes )
          .then( respostas => {
            let qtdVezesAssistido = 0;
            let assistido = 'Não';
            if(respostas[2].length > 0){
              qtdVezesAssistido = respostas[2].length;
              assistido = 'Sim';
            }
            const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota } = respostas[0];
            let episodios = new Episodios( id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota )
            episodios.qtdVezesAssistido = qtdVezesAssistido;
            episodios.assistido = assistido;
            this.setState( {
              episodios,
              detalhes: respostas[1],
              objNota: respostas[2],
            } )
          })
      } 
    
      registrarNota = ( nota ) => {
        if(nota === null) {
          nota = 1
        }
        const { episodios } = this.state;
        episodios.marcarComoAssistido();
        if ( episodios.validarNota( nota ) ) {
          console.log(episodios)
          episodios.avaliar( nota ).then( () => {
            console.log(nota)
          })
        }
        this.setState((state) => {
          return { ...state, episodios };
        });
        this.episodiosApi.buscarNota( episodios.id )
          .then( resposta => {
            this.setState( {
              objNota: resposta
            } )
          } )
      }
    
      mediaNotas = ( objNotas ) => {
        const tamanho = objNotas.length;
        let total = 0;
        if( tamanho === 0 ) {
          return total;
        }
        objNotas.forEach(element => {
          total += element.nota;
        });
        return total / tamanho;
      }

    render () {
        const { episodios, detalhes, objNota } = this.state;
        return (
            <React.Fragment>
                 <div className='detalhes'>
                <Header/>
                <div className='cards'>
                { episodios && ( <EpisodioUi episodios={ episodios } /> )}
                {
                    detalhes ?
                    <React.Fragment>
                        <li className='lista-episodios lista-episodios-sinopse'> { detalhes.sinopse } </li>
                        <li className='lista-episodios'> { new Date( detalhes.dataEstreia ).toLocaleDateString() } </li>
                        <li className='lista-episodios'>IMDB: { detalhes.notaImdb * 0.5 } </li>
                        <Box component="fieldset" mb={3} borderColor="transparent">
                        <Typography component="legend">Sua nota:</Typography>
                        <Rating name="read-only" 
                                value={  objNota ? this.mediaNotas( objNota ) : 'N/D' } 
                                readOnly 
                                precision={0.5} 
                        />
                        </Box>
                        <Box component="fieldset" mb={3} borderColor="transparent">
                            <Typography component="legend">Avaliar:</Typography>
                            <Rating name="pristine" 
                                    value={1} 
                                    max={5}  
                                    onChange={ (event, newValue) => this.registrarNota( newValue ) } 
                                    precision={0.5} 
                            />
                        </Box>
                        
                    </React.Fragment> : null
                }
                </div>
                </div>
            </React.Fragment>
        )
    }
}