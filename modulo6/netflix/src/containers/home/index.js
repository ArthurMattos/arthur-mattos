import React, { Component } from 'react';
import './Home.scss';

import { EpisodioUi,
  ButtonUi,
  MensagemFlash,
  MeuInputNumero,
  Lista
} from '../../components';

import Mensagens from '../../constants/mensagens';
import { ListaEpisodios, EpisodiosApi } from '../../models';

export default class Home extends Component {
  constructor(props){
    super(props);
    this.episodiosApi = new EpisodiosApi();
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      mostrarMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    };
  } 

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        const episodiosDoServidor = respostas[ 0 ];
        const notasServidor = respostas[ 1 ];
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor, notasServidor );
        this.setState( state => { return { ...state, episodios: this.listaEpisodios.episodiosAleatorios } } );
      })
  }

  sortear() {
    const episodios = this.listaEpisodios.episodiosAleatorios;
    this.setState(( state ) => {
      return { ...state, episodios }
    })
  }

  salvarNota( { nota, erro } ) {
    this.setState( state => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    if( erro ) {
      return;
    }

    const { episodios } = this.state;
    let corMensagem, mensagem;
    
    if (episodios.validarNota( nota )) {
      episodios.avaliar( nota ).then(() => {
      corMensagem = 'verde';
      mensagem = Mensagens.SUCESSO.REGISTRO_NOTA;
      this.exibirMensagem ( {corMensagem, mensagem} );
    });
    } else {
      corMensagem = 'vermelho';
      mensagem = Mensagens.ERRO.NOTA_INVALIDA;
      this.exibirMensagem( { corMensagem, mensagem } );
    }
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState( state => {
      return {
        ...state,
        mostrarMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState( state => {
      return { 
        ...state,
        mostrarMensagem: devoExibir
      }
    });
  }

  marcarComoAssistido () {
    const { episodios } = this.state;
    episodios.marcarComoAssistido();
    this.setState (state => {
      return {...state, episodios}
    })
  }
  
  linha( item, i ) {
    return <ButtonUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
  }

  render(){
    const { episodios, mostrarMensagem, mensagem, corMensagem, deveExibirErro } = this.state;
    const { listaEpisodios } = this;

    return ( !episodios ? (
        <h3>Aguarde...</h3>
       ) : (
      <React.Fragment>
        <MensagemFlash
              atualizarMensagem={ this.atualizarMensagem }
              mensagem={ mensagem }
              cor={ corMensagem }
              exibir={ mostrarMensagem }
              segundos="5" />
        <header className="App-header"> 
        <ButtonUi link={{pathname: "/lista", state: { listaEpisodios } }} nome="Lista de notas"/>
        <ButtonUi link={{pathname: "/episodios", state: { listaEpisodios } }} nome="Todos os episódios"/>
        <EpisodioUi episodios={ episodios } />
        <Lista
              classeName="botoes"
              dados={ [
                  { cor: "vermelho", nome: "Próximo", metodo: this.sortear.bind( this ) },
                  { cor: "amarelo", nome: "Assistir", metodo: this.marcarComoAssistido }
              ] }
              funcao={ ( item, i ) => this.linha( item, i ) } />
          <MeuInputNumero
                  className="descricao-nota"
                  placeholder="Nota de 1 a 5"
                  mensagem="Qual a sua nota para esse episódio?"
                  obrigatorio={ true }
                  visivel={ episodios.assistido || false }
                  deveExibirErro={ deveExibirErro }
                  atualizarValor={ this.salvarNota.bind( this ) } />
        </header>
        </React.Fragment>
       )
    )
  };
}


