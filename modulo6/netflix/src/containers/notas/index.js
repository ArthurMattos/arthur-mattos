import React, { Component } from 'react';

import { ButtonUi, ListaEpisodiosUi } from '../../components';
import { EpisodiosApi, ListaEpisodios } from '../../models';

export default class ListaNotas extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      avaliados: []
    }
  }

  componentDidMount() {
    let { listaEpisodios } = this.props.location.state;
    if( listaEpisodios.avaliados ) {
      this.setState( {
        avaliados: listaEpisodios.avaliados
      } )
      return;
    }

    listaEpisodios = new ListaEpisodios();
    listaEpisodios._todos = this.props.location.state.listaEpisodios._todos;
    this.episodiosApi.buscarTodasNotas().then( notas => {
      listaEpisodios.atualizarNotas( notas );
      this.setState( {
        avaliados: listaEpisodios.avaliados
      } )
    })
  }

  render() {
    const { avaliados } = this.state;

    return(
      <React.Fragment>
        <header className="App-header">
          <ButtonUi link="/" nome="Página Inicial" />
          <ListaEpisodiosUi listaEpisodios={ avaliados } />
        </header>
      </React.Fragment>
    )
  }

}