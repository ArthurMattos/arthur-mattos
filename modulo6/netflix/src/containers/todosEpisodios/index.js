import React, { Component } from 'react';
import { EpisodiosApi, ListaEpisodios } from '../../models';
import { ListaEpisodiosUi, ButtonUi, Header } from '../../components';

export default class TodosEpisodios extends Component {
    
    constructor( props ) {
        super( props );
        this.episodiosApi = new EpisodiosApi();
        this.state = {
          ordenacao: () => { },
          tipoOrdenacaoDataEstreia: 'ASC',
          tipoOrdenacaoDuracao: 'ASC',
          listaEpisodios: []
        }
      }
    
    componentDidMount() {
      const requisicoes = [
        this.episodiosApi.buscar(),
        this.episodiosApi.buscarTodosDetalhes(),
        this.episodiosApi.buscarTodasNotas()
      ];
    
      Promise.all( requisicoes )
        .then( respostas => {
          let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
          this.setState( state => {
            return {
              ...state,
              listaEpisodios: listaEpisodios._todos.concat([])
            }
          })
        })
    }
    
      filtrarPorTermo = e => {
        const requisicoes = [
          this.episodiosApi.buscar(),
          this.episodiosApi.buscarTodasNotas(),
          this.episodiosApi.buscarTodosDetalhes(),
        ];
    
        Promise.all(requisicoes).then((respostas) => {
          const episodiosDoServidor = respostas[0];
          const notasServidor = respostas[1];
          const detalhesDoSeridor = respostas[2];
          const listaEpisodiosNova = new ListaEpisodios(
            episodiosDoServidor,
            notasServidor,
            detalhesDoSeridor
          );
          const episodio = listaEpisodiosNova.episodiosAleatorios;
          const id = episodio.id;
          this.setState((state) => {
            return { ...state, id, listaEpisodios: listaEpisodiosNova._todos };
          });
        });
    
        if (e.key === "Enter") {
          const termo = e.target.value;
          this.episodiosApi.filtrarPorTermo(termo).then((resultados) => {
            let lista = [];
            resultados.forEach((element) => {
              this.state.listaEpisodios.forEach((ep) => {
                if (element.id === ep.id) {
                  lista.push(ep);
                }
              });
            });
            this.setState((state) => {
              return { ...state, listaEpisodios: lista };
            });
          });
        }
      }
    
      linha( item, i ) {
        return <ButtonUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
      }
    
      render() {
        const { listaEpisodios } = this.state;
        listaEpisodios.sort( this.state.ordenacao );
    
        return (
          <React.Fragment>
              <Header metodo={ this.filtrarPorTermo.bind( this ) } />
              <div className="cards">
              <ListaEpisodiosUi listaEpisodios={ listaEpisodios } />
              </div>
          </React.Fragment>
        )
      }
    
    }