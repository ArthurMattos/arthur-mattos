import EpisodiosApi from "./episodiosApi";

export default class Episodios {
    constructor ( id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota, detalhe) {
        this.id = id;
        this.nome = nome;
        this.duracao = duracao;
        this.temporada = temporada;
        this.ordem = ordemEpisodio;
        this.thumbUrl = thumbUrl;
        this.qtdAssistido = 0;
        this.episodiosApi = new EpisodiosApi;
        this.nota = ( nota || {} ).nota;
        this.dataEstreia = ( detalhe || {} ).dataEstreia;
    }

    get duracaoEmMin () {
        return `${ this.duracao } min`;
    }

    get temporadaEpisodio () {
        return `${ this.temporada.toString().padStart( 2, 0 ) }/${ this.ordem.toString().padStart( 2, 0 )}`;
    }

    avaliar( nota ) {
        this.nota = parseInt( nota );
        return this.episodiosApi.registrarNota( { nota: this.nota, episodioId: this.id } );   
    }

    marcarComoAssistido () {
        this.assistido = true;
        this.qtdAssistido++;
    }

    validarNota( nota ) {
        return (nota >= 1 && nota <= 5);
      }
}