import axios from 'axios';

const address = 'http://localhost:9000/api/';

const _get = address => new Promise( ( resolve, reject) => axios.get( address ).then( response => resolve(response.data)));
const _post = (address, dados) => new Promise( ( resolve, reject) => axios.post( address, dados ).then( response => resolve(response.data)));

export default class EpisodiosApi {
    
    async buscar() {
        return await _get( `${ address }episodios` );
      }

    async buscarTodosDetalhes( ) {
      return await _get( `${ address }detalhes` );
    }
    
    async buscarEpisodio( id ) {
      const response = await _get( `${ address }episodios?id=${ id }` );
      return response[ 0 ];
    }
    
    async buscarDetalhes ( id ) {
      const response = await _get(`${ address }episodios/${ id }/detalhes` );
      return response [ 0 ];
    }

    buscarNota ( id ) {
      return _get(` ${ address }notas?episodioId=${ id }`);
    }

    buscarTodasNotas() {
      return _get( `${ address }notas` );
    }

      async registrarNota( { nota, episodioId } ) {
        const response = await _post( `${ address }notas`, { nota, episodioId } );
        return response[ 0 ];
    }
  
      filtrarPorTermo( termo ) {
        return _get( `${ address }detalhes?q=${ termo }` )
    }
  
  }