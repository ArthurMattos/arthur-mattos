/* eslint-disable no-undef */
/* eslint-disable array-callback-return */
/* let dadosPokemon = document.getElementById('dadosPokemon');
let pokeApi = new PokeApi();
let consultarJson = pokeApi.buscarEspecifico(112);
consultarJson.then(pokemon => {
        let nome = dadosPokemon.querySelector(".nome");
        nome.innerHTML = pokemon.name;

        let imgPokemon = dadosPokemon.querySelector(".thumb");
        imgPokemon.src = pokemon.sprites.front_default;

        console.log(`Pokemon: ${pokemon.name}`)
    }
); */

const pokeApi = new PokeApi();
/* pokeApi
    .buscarEspecifico(112)
    .then(pokemon => {
        let poke = new Pokemon(pokemon)
        renderizar(poke);
    }
); */

let idInicial = 0;
const pokemonUsed = [];

buscarPokemon = () => {
  const idPokemon = document.getElementById( 'idPokemon' ).value;
  const erro = document.getElementById( 'erro' );
  pokeApi
    .buscarEspecifico( idPokemon )
    .then( pokemon => {
      erro.innerHTML = '';
      const poke = new Pokemon( pokemon )
      renderizar( poke );
    }, err => {
      erro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}

estouComSorte = () => {
  randomNumber = ( min, max ) => Math.floor( Math.random() * ( max - min + 1 ) ) + min
  const buscaId = randomNumber( 1, 893 );
  const numUsed = pokemonUsed.filter( num => num === buscaId );
  if ( numUsed.length > 0 ) {
    return;
  }
  pokemonUsed.push( buscaId );

  pokeApi
    .buscarEspecifico( buscaId )
    .then( pokemon => {
      erro.innerHTML = '';
      const poke = new Pokemon( pokemon )
      renderizar( poke );
    }, err => {
      const erro = document.getElementById( 'erro' );
      erro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}

renderizar = ( pokemon ) => {
  if ( idInicial === pokemon.pokeId ) {
    return;
  }

  const dadosPokemon = document.getElementById( 'pokedex' );
  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = `Name: ${ pokemon.nome }`;

  const pokeId = dadosPokemon.querySelector( '.pokeId' );
  pokeId.innerHTML = `Nº: ${ pokemon.pokeId }`;
  idInicial = pokemon.pokeId;

  const imgPokemon = dadosPokemon.querySelector( '.thumb' );
  imgPokemon.src = pokemon.imagem;
  imgPokemon.alt = `Img ${ pokemon.nome }`

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Height: ${ pokemon.altura }`;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Weight: ${ pokemon.peso }`;

  const tipos = dadosPokemon.querySelector( '.tipo' );
  tipos.innerHTML = 'Type:'
  const ul = document.createElement( 'ul' );
  tipos.appendChild( ul );
  pokemon.tipos.map( tipo => {
    const li = document.createElement( 'li' );
    li.innerHTML = `${ tipo.type.name } `;
    ul.appendChild( li );
  } );

  const estatistica = dadosPokemon.querySelector( '.estatistica' );
  estatistica.innerHTML = 'Stats:';
  const ulStat = document.createElement( 'ul' );
  estatistica.appendChild( ulStat );
  pokemon.estatistica.map( stats => {
    const li = document.createElement( 'li' );
    li.innerHTML = `
         ${ stats.stat.name }
        *   ${ stats.base_stat }%
        *   ${ stats.effort }%
        `;
    ulStat.appendChild( li );
  } );
}
