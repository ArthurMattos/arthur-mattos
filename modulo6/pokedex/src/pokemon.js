class Pokemon {// eslint-disable-line no-unused-vars
  constructor( pokemon ) {
    this._nome = pokemon.name;
    this._imagem = pokemon.sprites.front_default;
    this._pokeId = pokemon.id;
    this._altura = pokemon.height;
    this._peso = pokemon.weight;
    this._tipos = pokemon.types;
    this._estatistica = pokemon.stats;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get pokeId() {
    return this._pokeId;
  }

  get altura() {
    return `${ this._altura * 10 } cm`;
  }

  get peso() {
    return `${ this._peso / 10 } kg`;
  }

  get tipos() {
    return this._tipos;
  }

  get estatistica() {
    return this._estatistica;
  }
}
