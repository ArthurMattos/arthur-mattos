import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const ButtonUi = ({classe, metodo, nome, link } ) => 
    <React.Fragment>
        <button className={`btn ${ classe }`} onClick={ metodo } > 
        { link ? <Link to={ link }>{ nome }</Link> : nome }
        </button>
    </React.Fragment>

    ButtonUi.propTypes = {
        nome: PropTypes.string.isRequired,
        metodo: PropTypes.func,
        classe: PropTypes.string
    }

export default ButtonUi;