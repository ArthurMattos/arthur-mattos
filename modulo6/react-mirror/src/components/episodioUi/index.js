import React, { Component } from 'react';

export default class EpisodioUi extends Component {
  render(){
    const { episodios } = this.props;
    
    return (
      <React.Fragment>
        <h2>{ episodios.nome }</h2>
        <img src={ episodios.thumbUrl } alt={ episodios.nome } />
        <span>Já Assisti: { episodios.assistido ? 'Sim' : 'Não' }, { episodios.qtdAssistido } Vez(es)</span>
        <span>Duração: { episodios.duracaoEmMin }</span>
        <span>Temporada / Episódio: { episodios.temporadaEpisodio }</span>
        <span>Nota: {episodios.nota} </span>
      </React.Fragment>
    )
  }
}