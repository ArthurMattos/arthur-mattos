import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';


import Home from './home';
import ListaNotas from './notas';
import DetalhesEpisodios from './detalhesEpisodios';
import TodosEpisodios from './todosEpisodios';

export default class App extends Component {
  
  render(){
    return (
      <div className="App">
      <Router>
        <Route path="/" component={ Home } />
        <Route path="/lista" exact component={ ListaNotas } />
        <Route path="/episodios" exact component={ TodosEpisodios } />
        <Route path="/episodios/:id" exact component={ DetalhesEpisodios } />
      </Router>
      </div>
    )
  };
}

//const listaNotas = () => <h2> Lista de Notas </h2>

