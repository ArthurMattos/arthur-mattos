import React, { Component } from 'react';
import { EpisodiosApi, ListaEpisodios } from '../../models';
import { ListaEpisodiosUi, ButtonUi, Lista, CampoBusca } from '../../components';


export default class TodosEpisodios extends Component {
    
    constructor( props ) {
        super( props );
        this.episodiosApi = new EpisodiosApi();
        this.state = {
          ordenacao: () => { },
          tipoOrdenacaoDataEstreia: 'ASC',
          tipoOrdenacaoDuracao: 'ASC',
          listaEpisodios: []
        }
      }
    
      componentDidMount() {
        if( this.props.location.state ) {
          this.setState( state => {
            return {
              ...state,
              listaEpisodios: this.props.location.state.listaEpisodios._todos.concat([])
            }
          })
          return;
        }
    
        const requisicoes = [
          this.episodiosApi.buscar(),
          this.episodiosApi.buscarTodosDetalhes(),
          this.episodiosApi.buscarTodasNotas()
        ];
    
        Promise.all( requisicoes )
          .then( respostas => {
            let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
            this.setState( state => {
              return {
                ...state,
                listaEpisodios: listaEpisodios._todos.concat([])
              }
            })
          })
      }
    
      alterarOrdenacaoParaDataEstreia = () => {
        const { tipoOrdenacaoDataEstreia } = this.state;
        this.setState( {
          ordenacao: ( a, b ) =>  ( tipoOrdenacaoDataEstreia === 'ASC' ? a : b ).dataEstreia  - ( tipoOrdenacaoDataEstreia === 'ASC' ? b : a ).dataEstreia,
          tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === 'ASC' ? 'DESC' : 'ASC'
        } );
      }
    
      alterarOrdenacaoParaDuracao = () => {
        const { tipoOrdenacaoDuracao } = this.state;
        this.setState( {
          ordenacao: ( a, b ) => ( tipoOrdenacaoDuracao === 'ASC' ? a : b ).duracao - ( tipoOrdenacaoDuracao === 'ASC' ? b : a ).duracao,
          tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === 'ASC' ? 'DESC' : 'ASC'
        } );
      }
    
      filtrarPorTermo = evt => {
        const termo = evt.target.value;
        this.episodiosApi.filtrarPorTermo( termo )
          .then( resultados => {
            this.setState({
              listaEpisodios: this.state.listaEpisodios.filter( e => resultados.some( r => r.episodioId === e.id ) )
            })
          } )
      }
    
      linha( item, i ) {
        return <ButtonUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
      }
    
      render() {
        const { listaEpisodios } = this.state;
        listaEpisodios.sort( this.state.ordenacao );
    
        return (
          <React.Fragment>
            <header className="App-header">
              <h1>Todos Episódios</h1>
              <CampoBusca atualizaValor={ this.filtrarPorTermo } placeholder="Ex.: ministro" />
              <Lista
                  classeName="botoes"
                  dados={ [
                      { cor: "verde", nome: "Ordenar por data de estreia", metodo: this.alterarOrdenacaoParaDataEstreia },
                      { cor: "azul", nome: "Ordenar por duração", metodo: this.alterarOrdenacaoParaDuracao }
                  ] }
                  funcao={ ( item, i ) => this.linha( item, i ) } />
              <ListaEpisodiosUi listaEpisodios={ listaEpisodios } />
            </header>
          </React.Fragment>
        )
      }
    
    }