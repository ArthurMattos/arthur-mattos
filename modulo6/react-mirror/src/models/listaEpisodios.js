import Episodios from "./episodios";

function _sortear( min, max ){
    min = Math.ceil(min);
    max = Math.floor( max );
    return Math.floor( Math.random() *  (max - min ) ) + min;
}

export default class ListaEpisodios {
   constructor( episodiosDoServidor = [], notasDoServidor = [], detalhesDoServidor = [] ){
		this._todos = episodiosDoServidor.map( elem => new Episodios( elem.id, elem.nome, elem.duracao, elem.temporada, elem.ordemEpisodio, elem.thumbUrl ) );
		this.atualizarNotas( notasDoServidor );
		this.atualizarDetalhes ( detalhesDoServidor );
	}

	get avaliados() {
		return this._todos.filter( e => e.nota ).sort( ( a,b ) => a.temporada - b.temporada || a.ordemEpisodio - b.ordemEpisodio );
	}

	get episodiosAleatorios() {
		const indice = _sortear( 0, this._todos.length );
		return this._todos[ indice ];
	}

	atualizarNotas( notasDoServidor ) {
		this._todos = this._todos.map( episodios => {
			const nota = notasDoServidor.filter( n => n.episodioId === episodios.id ).sort( ( a,b ) => b.id - a.id )[0];
			episodios.nota = ( nota || {} ).nota;
			return episodios;
		} )
	}

	atualizarDetalhes( detalhesDoServidor ) {
			this._todos = this._todos.map( episodio => {
				const detalhe = detalhesDoServidor.find( d => d.episodioId === episodio.id );
				episodio.notaImdb = ( detalhe || {} ).notaImdb;
				episodio.sinopse = ( detalhe || {} ).sinopse;
				episodio.dataEstreia = new Date( ( detalhe || {} ).dataEstreia );
				return episodio;
			} )
		}

}