let circulo = {
    raio: 2,
    tipoCalculo: "A"
}

function calcularCirculo({raio, tipoCalculo:tipo} ) {
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow( raio, 2 ) : 2 * Math.PI * raio);
}

console.log(calcularCirculo(circulo))

function naoBissexto (ano) {
    return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true;
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

function somaPares (array) {
    let somatoria = 0;

    for (let i = 0; i < array.length; i++) {
        if (i % 2 == 0) {
        somatoria += array[i];
        }
    }

    return somatoria;
}

console.log(somaPares[1, 56, 4.34, 6, -2]);

function adicionar (op1) {
    return function(op2) {
        return op1 + op2;
    }
}

console.log(adicionar(3)(4))

//let adicionar = op1 => op2 => op1 + op2;




