console.log("Para a demonstração de que inverdades não foram inferidas por aquele responsável pelo ensino!");

let nome2="qualquer nome 2: eletric boogaloo";

console.log(nome2);

const cpf = "00000000000";

console.log(cpf);

const pessoa = {
    nome: "Arthur"
};

Object.freeze(pessoa);

pessoa.nome = "Não Arthur";
pessoa.idade = 19;
pessoa.status = "Lindo";

console.log(pessoa);

let nome= "Arthur";
let idade=19;
let semestre=2;
let notas=[8, 7, 8, 10];    

function funcaoCriacaoObjetoAluno(nomeExt, idadeExt, semestreExt, notas) {
    const aluno = {
        nome: nomeExt,
        idade: idadeExt,
        semestre: semestreExt
    }

    function verificaAprovacao(notas) {
        if (notas.length == 0) {
            return "Sem notas";
        }
        
        let somatoria = 0;
        for (let i = 0; i < notas.length; i++) {
            somatoria += notas[i];
        }

        return somatoria/notas.length > 7 ? "Aprovado" : "Reprovado";
    }

    aluno.status = verificaAprovacao(notas);

    console.log(aluno);
    console.log(" --> Interno");
    return aluno;
}

const aluno = funcaoCriacaoObjetoAluno(nome, idade, semestre, notas);

console.log(aluno);
console.log(" --> Externo");

