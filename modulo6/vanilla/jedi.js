class Jedi {
    //_classePersonagem = "Jedi";

    constructor (nome) {
        this.nome = nome;
        this._estaMorto = false;
    }

    matarJedi() {
        this._estaMorto = true;
    }

    atacarComSabre () {
        console.log("Ataquei");
    }

    get retornarNome () {
        return this.nome;
    }

    get estaMorto () {
        return this._estaMorto;
    }
}

let sky = new Jedi ("Luke");

sky.atacarComSabre();
sky.matarJedi();

console.log(sky.retornarNome);
console.log(sky.estaMorto);