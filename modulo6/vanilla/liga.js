class Jogadores {
    constructor (nome, numero) {
        this._nome = nome;
        this._numero = numero;
        this._capitao = false;
    }

    get nome () {
        return this.nome;
    }

    get numero () {
        return this.numero;
    }

    virarCapitao = () => {
        this.capitao = true;
    }
}

class Partidas {
    constructor (mandante, visitante, placar) {
        this._mandante = mandante;
        this._visitante = visitante;
        this._placar = placar;
    }

    get mandante () {
        return this.mandante;
    }    

    get visitante () {
        return this.visitante;
    }

    get placar () {
        return this.placar;
    }
}

class Time {

    constructor (nome, tipoEsporte, status, liga) {
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = status;
        this._liga = liga;
        this._jogadores = [];
    }

    get retornarNome () {
        return this._nome;
    }
    
    get retornarTipoEsporte () {
        return this._tipoEsporte;
    }

    get retornarStatus () {
        return this._status;
    }
    
    get retornarLiga () {
        return this._liga;
    }

    adicionaJogador = (jogador) => {
        this._jogadores.push(jogador);
    }
    
    buscarJogadorPorNome = (nomeJogador) => {
        return this._jogadores.filter( jogador => jogador.nome === nomeJogador);
    }

    buscarJogadorPorNumero = (numeroJogador) => {
        return this._jogadores.filter( jogador => jogador.numero === numeroJogador);
    }

    historicoPartidas = () => {

    }


}

let flamengo = new Time ("Flamengo", "Futebol", "Ativo", "Não sei");

let vitinho = new Jogadores ("Vitinho", 10);

flamengo.adicionaJogador(vitinho);


console.log(flamengo);
console.log(flamengo.buscarJogadorPorNome("Vitinho"));
console.log(flamengo.buscarJogadorPorNumero(10));